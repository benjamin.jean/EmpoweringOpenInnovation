---
title:  ’Empowering Open Innovation. Du rôle et de la place des modèles ouverts (v.0.9)’
author:
- Jean, Benjamin (dir.)
- Pellegrini, François (dir.)
- Charleux, Amel
- Gautier, Sophie
- Kassem, Laure
tags: [guide, innovation, open, ouvert, modèle]
abstract: |
  Ce guide vise à présenter l’Innovation Ouverte comme un processus de transformation systémique et global, qui repose sur la rencontre d’une multitude d’acteurs et conduit progressivement à les faire tous évoluer. À l’échelle d’une organisation, un tel infléchissement débute nécessairement par l’introduction d’outils techniques et de pratiques managériales tout autant en interne qu’en externe afin de permettre la collaboration.
  
  À cette fin, le guide met en avant le rôle structurant des technologies et modèles ouverts, représentés par l’Open Data, l’Open Source et l’Open Hardware, dans le déploiement de l’Innovation Ouverte. Le recours à ces mécanismes d’ouverture conduit à la pérennisation des collaborations nouées entre des acteurs hétérogènes et consolide durablement leurs relations. Nous verrons qu’elles apportent à l’Innovation Ouverte le bénéfice d’une structure en réseau de type horizontal qui dépasse les limites des modèles pyramidaux classiques. Ces outils favorisent ainsi l’apparition d’innovations disruptives aussi bien technologiques que sociétales. 
...

Empowering Open Innovation&nbsp;: du rôle et de la place des modèles ouverts

<!--
Notes pour  la suite :
- une relecture ortho-typo s'impose. J'ai relevé encore beaucoup de coquilles.
- prenez modèle sur les liens que j'ai établi dans le snotes de bas de page : beaucoup de liens renvoyaient en fait à des articles, il faut citer correctement. 
- utiliser le formatage markdown dont les principes sont expliqués ici : https://daringfireball.net/projects/markdown/syntax
- on utilise une variante de markdown compatible avec pandoc : prenez modèle sur les notes de bas de page, par exemple, dans le texte existant.
- écrire en respectant la typographie : utilisation des guillemets français (AltGr + w/x), encodage utf8 obligatoire, espaces insécables soit en utf8 soit en les encodant en html (&nbsp;), respecter la typo des listes.
- quelques principes, par exemple lorsqu'on cite un extrait on le place entre guillemets et sans italiques.
- beaucoup de titres sont trop long : il faudrait penser à les raccourcir ! 
-->


<!-- LE DOCUMENT COMMENCE ICI -->


«&nbsp;Imagination is the Discovering Faculty, pre-eminently. It is that which penetrates into the unseen worlds around us, the worlds of Science.&nbsp;» 

Ada Lovelace


# Introduction

En 2015, la NASA lançait le *Technology Transfer Program*, donnant accès à son portefeuille de plus de 1200 brevets en vue de faciliter l’obtention de licences sans paiement préalable. Identifiant là une véritable opportunité pour son écosystème, la NASA prouvait encore une fois que l’innovation passe par l’ouverture et la collaboration. Elle permettait ainsi aux entreprises du secteur privé de bénéficier des résultats de la recherche et de s’en saisir de manière indifférenciée grâce à la non-exclusivité des licences. La mutualisation entre secteur public et privé apparaît ainsi comme un moyen efficace de décupler le potentiel des inventions et de favoriser les nouveaux entrants sur le marché.

Cette démarche s’inscrit dans un mouvement plus large, puisqu’un certain nombre de grands groupes privés se sont inscrits dans des logiques d’ouverture similaires. C’est le cas par exemple de Tesla Motors, qui a décidé en 2014 d’ouvrir ses brevets relatifs à sa voiture électrique[^tesla].

[^tesla]: Voir «&nbsp;Tous nos brevets vous appartiennent —&nbsp;Tesla Motors&nbsp;», [alireailleurs.tumblr.com](http://alireailleurs.tumblr.com/post/88950532518/tous-nos-brevets-vous-appartiennent-tesla-motors).

Il apparaît alors que l’innovation ne peut plus être pensée à l’échelle d’une seule organisation, mais doit être mesurée à l’aune de la société dans son ensemble.

La spécialisation croissante des filières, des disciplines et des métiers a conduit à la segmentation des marchés, participant à exacerber la logique concurrentielle&nbsp;: chacun cherche à progresser pour s’imposer individuellement. Il en a résulté une innovation en silos au sein de laquelle les organisations se sont concentrées sur leur seul domaine de compétences et ont réduit progressivement leurs connectivités avec l’extérieur, en cloisonnant leurs frontières. L’innovation a ainsi un temps été réservée aux seuls départements de recherche et développement. Chaque organisation cherchait alors à capter les meilleurs talents en vue d’en tirer un avantage concurrentiel.

Or, la combinaison de plusieurs facteurs a grandement érodé cette logique. En premier lieu, la mondialisation des échanges a d’une part décuplé les débouchés pour les innovations et d’autre part, la diversification des acteurs s’est accentuée. Puis, le développement du numérique a entraîné une prolifération des données, une dissémination des savoirs, mais aussi une augmentation du rythme de production d’innovations,  en même temps que la réduction du cycle de vie des produits. Les outils favorisant le travail collaboratif se sont alors perfectionnés afin de répondre à ces évolutions. La mobilité et la dispersion des compétences se sont accrues dans une société où les contributions des individus répondent autant à des logiques personnelles que professionnelles, avec même, dans ce dernier cas, une vraie préoccupation de l’impact sociétal.

Face à ces changements, chaque acteur doit repenser son modèle organisationnel. L’Innovation Ouverte constitue à la fois une réponse adaptée aux mutations des écosystèmes ainsi qu’aux enjeux sociétaux, et un impératif face à la demande croissante des clients. Posant le principe du travail collaboratif, le modèle ouvert bouleverse la conception traditionnelle de l’innovation, avec l’idée de créer mieux avec et grâce aux autres. Ceci rend possible une réduction des coûts par mutualisation mais aussi de tirer profit de cette multitude en vue d’optimiser la diffusion d’une technologie. Dans ce contexte, chaque acteur augmente sa capacité à bénéficier de la créativité, de l’intelligence et des contributions environnantes, stimulant ainsi l’innovation et accélérant le développement de nouveaux produits et services. Pour ce faire, l’entreprise est amenée à s’ouvrir vers l’extérieur afin de prendre en compte les flux entrants et sortants de connaissances, ce qui se traduit par le recours à un certain nombre de pratiques telles que les *spin-off*, les alliances et les partenariats de tout genre. Le recours aux technologies ouvertes (Open Data, Open Source et Open Hardware) est transversal, de telle sorte qu’il imprègne les dispositifs cités de leurs valeurs. De cette manière, les processus d’innovation gagnent en souplesse et en agilité, permettant de réduire le *time-to-market* grâce à une réactivité accrue. Enfin, l’Innovation Ouverte, en modifiant les rapports tant au sein de l’organisation que dans les relations avec les acteurs externes, impulse d’importants changements sociaux, voire sociétaux.

Aujourd’hui, l’Innovation Ouverte intéresse de plus en plus les acteurs de tout secteur, conscients qu’ils ne peuvent plus maintenir leur compétitivité en innovant seuls et souhaitant aborder le marché sous un angle nouveau. Néanmoins, si l’Innovation Ouverte tend à se généraliser, elle n’est pas en mesure à ce jour de déployer pleinement son potentiel dès lors que peu d’acteurs entrent réellement dans une démarche d’ouverture systématique et stratégique. En effet, au-delà des flux de savoirs, ce modèle suppose d’aller plus loin dans l’ouverture, notamment en se positionnant clairement vis-à-vis de l’Open Data, de l’Open Source et de l’Open Hardware, seuls courants à même de conférer au dispositif une pérennité indispensable pour entraîner la confiance des autres acteurs économiques et de systématiser la collaboration. Les organisations doivent mener une véritable réflexion globale quant à leurs aspirations et leur stratégie de développement -- désormais ouverte vers l’extérieur. Ceci passe par une modification profonde des relations avec les partenaires, les distributeurs, les clients et les salariés.


## Objectif du guide

Ce guide vise à présenter l’Innovation Ouverte comme un processus de transformation systémique et global, qui repose sur la rencontre d’une multitude d’acteurs et conduit progressivement à les faire tous évoluer. À l’échelle d’une organisation, un tel infléchissement débute nécessairement par l’introduction d’outils techniques et de pratiques managériales tout autant en interne qu’en externe afin de permettre la collaboration.

À cette fin, le guide met en avant le rôle structurant des technologies et modèles ouverts, représentés par l’Open Data, l’Open Source et l’Open Hardware, dans le déploiement de l’Innovation Ouverte. Le recours à ces mécanismes d’ouverture conduit à la pérennisation des collaborations nouées entre des acteurs hétérogènes et consolide durablement leurs relations. Nous verrons qu’elles apportent à l’Innovation Ouverte le bénéfice d’une structure en réseau de type horizontal qui dépasse les limites des modèles pyramidaux classiques. Ces outils favorisent ainsi l’apparition d’innovations disruptives aussi bien technologiques que sociétales. 

\part{Appréhender l’Innovation Ouverte dans une perspective d’écosystème} 

L’écosystème représente une communauté d’acteurs et la somme de leurs interactions. Sa principale raison d’être est la coopération et son développement repose sur l’innovation, ce qui en fait  un espace privilégié pour le déploiement de stratégies d’Innovation Ouverte. C’est un cadre institutionnel, aux frontières mouvantes, qui permet le partage et la circulation des idées grâce aux dynamiques positives qu’il provoque. Il est ainsi porteur d’opportunités pour les entreprises qui en font partie.

De nombreux projets s’appuyant sur cette stratégie collective mobilisent les outils actuels que sont l’Open Data, l’Open Source et l’Open Hardware pour mener à bien la collaboration. Ces exemples, que nous développerons par la suite, participent, chacun à son échelle, à la définition, mais aussi à la diffusion des valeurs de l’Innovation Ouverte qui apparaît comme seule pertinente pour répondre à la pression croissante du marché tout en maintenant des modèles pérennes. Ils sont la preuve que l’innovation vient de l’usage et que le modèle ouvert, par son déploiement systématique au sein des écosystèmes, saura s’imposer comme norme.

# Les bénéfices collectifs d’une Innovation Ouverte 

L’Innovation Ouverte n’a de sens qu’en présence d’une multitude d’acteurs. Caractérisant l’adage selon lequel l’« union fait la force », elle introduit l’idée selon laquelle une pluralité d’acteurs hétérogènes peut définir et poursuivre des objectifs communs sans que l’intérêt individuel de chacun soit remis en cause.

Ce constat part notamment de l’idée que l’intelligence n’est plus concentrée au sein d’une seule organisation, mais au contraire de plus en plus répartie —&nbsp;plus ou moins équitablement&nbsp;— entre un ensemble d’acteurs fortement incités à collaborer et mutualiser pour obtenir une innovation de «&nbsp;départ&nbsp;» importante. La notion même de secteur est fortement remise en cause par une transversalité croissante qui s’impose, le bénéfice final d’une innovation pouvant se révéler parfois considérablement plus grand au sein d’autres domaines ou secteurs.

Les bénéfices liés à l’Innovation Ouverte résident alors dans la double qualité contributeur / utilisateur, vecteur d’un cercle vertueux. En effet, l’Innovation Ouverte repose sur l’enrichissement mutuel à la fois&nbsp;:

   * de la technologie, de la création, de l’invention, du jeu de données en cause&nbsp;: les contributeurs et utilisateurs ont la possibilité de les réutiliser librement, de les modifier en vue de les améliorer, ce qui souligne l’aspect participatif du dispositif&nbsp;;
   * de l’écosystème en question, voire de ceux qui lui sont adjacents&nbsp;: les contributeurs, les utilisateurs, les organisations publiques ou privées sont également destinataires de ces évolutions, sources de progrès. Il y a un aller-retour permanent entre ces deux éléments, de telle sorte que les évolutions apparues dans ce cadre profitent à la société dans son ensemble. 

## L’Innovation Ouverte, des enjeux économiques aux bénéfices des usagers

L’Innovation Ouverte, pensée à l’échelle d’un écosystème, suggère une visée fédératrice large. Elle invite les acteurs, et particulièrement les entreprises, à se projeter au-delà du cadre strict de l’organisation pour intégrer leurs projets dans une dynamique sociétale et une économie globalisée. Cela suppose d’intégrer dans le processus d’innovation non seulement des entreprises, mais aussi —&nbsp;et finalement surtout&nbsp;— les usagers. Les usagers sont la cible des projets d’innovation. C’est pour leurs besoins qu’on imagine de nouvelles solutions. Dans les modèles classiques d’innovation, on définit le cahier des charges en fonction de ces usagers puis on les laisse de côté une fois le projet lancé. En Innovation Ouverte, ces mêmes usagers peuvent continuer à jouer un rôle à part entière en ayant la possibilité de contribuer directement à la construction de la solution. Plus largement, les stratégies d’ouverture ont rendu possible la transformation d’initiatives individuelles en succès d’ampleur internationale à l’image des projets tels que le noyau Linux ou l’encyclopédie Wikipédia. L’innovation revêt alors une dimension nouvelle pour les usagers.

## L’Innovation Ouverte&nbsp;: les usagers au coeur de succès collectifs ?

Wikipédia, le noyau Linux, Git, OpenStreetMap, Firefox et bien d’autres encore sont des initiatives dont le succès repose sur le choix d’un modèle ouvert. En effet, si ces dernières ont rassemblé autant de personnes, ce n’est que parce qu’elles ont réussi à associer à une ambition forte des principes inspirant la confiance de tous. 

C’est ainsi le cas pour l’Open Content (Wikipédia), l’Open Data (OpenStreetMap) et l’Open Source (le noyauLinux, Git, Firefox). Ces projets sont devenus les étendards de l’Innovation Collective et Ouverte puisqu’ils participent à en élaborer les principes en même temps qu’ils en constituent les meilleurs vecteurs de diffusion. Par l’ampleur de leur succès et l’innovation qu’ils apportent, ils ont bouleversé les postulats sur lesquels reposait le développement de nos économies en démontrant les bienfaits des modèles ouverts et collaboratifs.


### Le noyau Linux

Un des plus emblématiques succès en la matière est celui du noyau Linux qui a joué un rôle essentiel dans le développement du Web. Avec l’appui de la communauté, Linus Torvalds a mis au point un programme qui est devenu la pièce centrale des systèmes d’exploitation GNU/Linux, inspirés d’Unix. Il est ainsi à l’origine d’une des transformations les plus significatives du secteur des nouvelles technologies. Sans ce noyau, le web et Android n’existeraient pas. Le produit est utilisé par la plupart des innovations majeures de notre société&nbsp;: dans la téléphonie, avec 65% des smartphones équipés, les télévisions, les voitures connectées et les ordinateurs embarqués, les satellites et les drones de tout genre, les bourses mondiales qui tournent grâce à ce noyau à 90%, sans oublier les 99% de super-calculateurs dans le monde qui l’utilisent également. Il s’agit d’«&nbsp;une brique essentielle de l’informatique moderne&nbsp;»[^briquessent]. La Fondation Linux recense aujourd’hui parmi ses contributeurs des entreprises telles que&nbsp;: IBM, Microsoft, Google, Facebook, Samsung, Huawei. 

[^briquessent]: Voir Eric L.B., «&nbsp;Linux a 25 ans&nbsp;: la folle histoire d’un logiciel qui a changé le monde&nbsp;», [01net.com](http://www.01net.com/actualites/linux-a-25-ans-la-folle-histoire-d-un-logiciel-qui-a-change-le-monde-1029586.html), 25/0822016.

La particularité du noyau Linux réside dans l’accessibilité de son code source &nbsp;: ouvert, librement modifiable et réutilisable. Selon Lawrence Lessig, célèbre professeur de droit à l’université d’Harvard, le modèle ouvert et collaboratif mis au point depuis 1991 pour le projet Linux a changé, pour toujours, l’économie du développement logiciel et plus encore, la manière de concevoir les systèmes d’exploitation. En effet, il s’agit bien là d’un des plus larges projets collaboratifs avec plus de 12&nbsp;000 contributeurs et 20&nbsp;millions de lignes de code. 

Selon Linus Torvalds, le succès de son initiative repose sur un certain «&nbsp;lâcher-prise&nbsp;» de sa part vis-à-vis du code, indispensable pour permettre l’enrichissement de ce dernier par des tiers[^enrichiss]. L’ouverture et le partage de l’ensemble des éléments permettant de maîtriser le projet sont donc les premières étapes pour la mise en place d’une innovation collective. Ce dernier explique ensuite que l’une des complexités de ce modèle d’innovation réside non pas dans la création d’une communauté, mais dans son maintien et sa croissance -- ce qui passe notamment par sa capacité à absorber de nouveaux contributeurs. Les cycles de développement courts (une nouvelle version tous les deux mois environ) ont en l’occurrence favorisé l’accroissement de la communauté [^accroicomm] dans la mesure où les contributeurs voient tout de suite leur participation incluse dans le produit. À l’origine, Linus Torvalds publiait de nouvelles versions tous les jours. Le rythme est ainsi un paramètre de succès important pour faire vivre une communauté de contributeurs ou plus largement un écosystème. Auparavant, les cycles de développement des programmes informatiques s’étalaient sur un à deux ans, ce qui était problématique pour fédérer une communauté.

[^enrichiss**]: Voir conférence TED 2016&nbsp;: *Linus Torvalds. The Mind Behind Linux*, [ted.com](http://www.ted.com/talks/linus_torvalds_the_mind_behind_linux).

[^accroicomm]: Voir page Wikipédia «&nbsp;Noyau Linux&nbsp;», [fr.wikipedia.org](https://fr.wikipedia.org/wiki/Noyau\_Linux).

Enfin, Linux est aussi un bel exemple d’Innovation Ouverte récursive&nbsp;: l’outil développé pour répondre au développement collaboratif et ouvert de Linux au travers d’une gestion fine des versions (nommé Git) a lui-même été conçu dans une logique collaborative et ouverte, permettant une adoption par de nombreux projets et un bénéfice décuplé. Devenu projet autonome très rapidement, Git a réussi à imposer un nouveau standard de développement qui favorise la collaboration, la contribution et la mutualisation entre tous les projets libres et Open Source.


### Wikipédia, une innovation majeure de la société de l’information

Aujourd’hui, site internet non-commercial le plus consulté, Wikipedia a changé la perception de la création et de la diffusion des connaissances en mettant en ligne une encyclopédie dont les contenus sont entièrement rédigés par les internautes et modifiables par ces derniers. À caractère universel et multilingue, il permet à chacun d’avoir un accès facilité à une information décentralisée créée par la communauté et à un contenu évolutif susceptible de s’adapter au contexte. Placés sous la licence Creative Commons BY-SA 3.0, les contenus proposés par l’encyclopédie Wikipédia évoluent sous une ligne éditoriale neutre. Cette liberté de contribution rend possible la multiplication des points de vue et surtout le contrôle des contenus par le plus grand nombre. Les articles font l’objet d’une expertise commune transdisciplinaire et sont accessibles gratuitement.

Disponible en 280 langues, Wikipédia fédère largement les lecteurs, qui sont de potentiels futurs contributeurs, et a su créer une communauté dynamique d’environ 80 000 personnes. Basé sur le modèle collaboratif, il fait ainsi prendre conscience au monde de la nécessité du partage en modernisant le concept même d’encyclopédie. À ce titre, Wikipédia constitue un des nouveaux usages liés au numérique, à partir desquels ont été conçues les licences Creative Commons.

<!--encart-->

**Les licences Creative Commons**

Elles visent à faciliter l’utilisation, la modification, la reproduction et la diffusion de contenus en vue d’enrichir l’écosystème de la connaissance. Elles se présentent sous forme d’options graduelles qui permettent d’osciller entre différents niveaux d’ouverture et de permissions. Elles empruntent ainsi aux principes du libre et de l’Open Source, preuve que l’Innovation Ouverte est un processus global.

<!--fin encart-->

Afin de garantir un accès gratuit, sans publicité, et des contributions libres, modérées exclusivement par la communauté, l’encyclopédie en ligne est adossée à une fondation&nbsp;: Wikimedia Foundation Inc. Celle-ci recueille les dons des internautes et garantit l’indépendance du site.

Wikipédia est une innovation qui introduit une richesse importante au regard de la quantité de connaissances et de médias mis à disposition, irradiant ainsi l’économie traditionnelle en offrant des opportunités et des services qui sinon auraient été payants, de moins bonne qualité ou simplement inexistants.



### OpenStreetMap, révolutionne  la cartographie

OpenStreetMap est le premier projet de carte collaborative créée à partir des contributions des internautes. Celles-ci proviennent principalement des cartographes, mais aussi des humanitaires présents sur les zones ayant subi des catastrophes naturelles. Les contributeurs viennent donc des quatre coins du monde ce qui permet un maintien régulier et à jour des données cartographiques locales.

Né en 2004, sous l’impulsion de Steve Coast, OpenStreetMap tire son intérêt de l’Open Data, favorisant ainsi la liberté de réutilisation. S’inscrivant dans une démarche sociétale la plupart du temps, OpenStreetMap est un support majeur pour les États et collectivités souhaitant partager leurs données cartographiques. C’est ainsi qu’a été créé en France un jeu de données nationales d’adresses à partir de la combinaison des données d’OpenStreetMap avec celles de la Poste, de l’État et de l’IGN (Institut national de l’information géographique et forestière). La Base Adresse Nationale (BAN) est elle-même ouverte et disponible sur le site data.gouv.fr.

Il s’agit également de proposer une alternative aux GAFA (Google, Apple, Facebook, Amazon), avec pour objectif la réappropriation des données par les usagers. En effet, avec les GAFA on assiste à l’appropriation d’un commun par des acteurs qui ont une force de frappe bien supérieure à celle du reste du marché et qui commercialisent un produit ou un service sans souvent contribuer en retour à ce commun. Néanmoins, ces entreprises ne sont pas exclues du projet puisque les données sont ouvertes de manière non-discriminatoire. De cette manière, elles peuvent enrichir lesdites données, sans possibilité de faire valoir un quelconque monopole. En effet, les données modifiées doivent donc être publiées sous les termes de la même licence (c’est-à-dire de la licence ODbL) ou sous une licence compatible. 

<!--encart-->

**La licence ODbL**

Elle fait partie des licences *Open Data Commons* à côté de la *Public Domain Dedication and License* (PDDL) et l’*Open Data Commons Attribution* (ODC-By). Ces dernières ont été créées par l’Open Knowledge Foundation. Il s’agit des premières licences rédigées spécifiquement pour l’Open Data en matière de bases de données et s’appuyant sur la législation européenne. Elles prévoient un encadrement complet en se fondant sur les droits d’auteur, le droit des contrats et le droit sui generis. En utilisant ces licences, le titulaire des droits concède de manière non-exclusive ses droits patrimoniaux portant sur une base de données. L’ODbL qui est plus particulièrement utilisée en France, est de type share-alike (ou copyleft), c’est-à-dire qu’elle s’étend à toute base de données reposant sur la base de données originaire dès lors que la base dérivée est exploitée publiquement.

<!-- fin encart-->

Toutefois, ne pouvant partager en retour leurs propres bases de données, compte tenu de la valeur ajoutée qu’ils y stockent, ces grands acteurs peinent à véritablement se saisir de ces mouvements d’ouverture. On assiste là très clairement au déploiement de deux logiques différentes&nbsp;: celle des GAFA qui souhaitent imposer un contrôle sur les données, et celle des communs qui suggère que les ressources sont accessibles à tous, sous les mêmes conditions. OpenStreetMap partage le second point de vue. Afin de promouvoir la complémentarité et de stimuler la concurrence sur le marché au bénéfice des utilisateurs, OpenStreetMap oeuvre au renforcement de cette initiative grâce au développement d’une stratégie communautaire favorisant l’inclusion du plus grand nombre.


## L’Innovation Ouverte&nbsp;: des usagers au coeur de succès économiques

L’innovation est au cœur des principaux grands changements qu’a connus l’Homme. Qu’il s’agisse de la machine à vapeur à l’origine de la Révolution industrielle ou de l’Internet à l’origine de la Révolution numérique. Elle participe à changer en profondeur non seulement les marchés, mais aussi et peut être surtout, les sociétés. Avec l’état de nos connaissances actuelles, leur accumulation et leur accès facilité grâce aux nouvelles Technologies de l’Information et de la Communication, le rythme des innovations s’est accéléré. Cette accélération est d’autant plus forte qu’elle se trouve portée par l’approche communautaire de l’Innovation Ouverte. 

L’Innovation Ouverte stimule l’économie en ouvrant les marchés à davantage de concurrence. C’est sur ce fondement, qu’en 2006 la *Court of Appeals for the Seventh Circuit* (États-Unis) a considéré que l’usage des licences libres n’était pas constitutif d’un comportement contraire aux lois antitrust (Wallace v. International Business Machines Corp.). Dans le prolongement de cette décision, le 23 mars 2010, la Cour constitutionnelle italienne a jugé que la préférence envers le logiciel libre est légitime et conforme au principe de la liberté de concurrence. 

Dans la même optique, le Gouvernement américain a imposé la création d’un groupement de brevets dans le domaine de l’aéronautique, ce qui aboutira à la mise en place de la MAA (*Manufacturer’s Aircraft Association*). Ce dernier a permis de créer des avions sans crainte de procès, mis fin aux querelles de brevets, favorisa l’essor de l’industrie aéronautique et profita à l’économie. En Espagne, un fait similaire va dans le sens de la promotion des logiciels libres. Ainsi, le CENATIC (Centre de référence pour l’application des technologies de l’information et de la communication basées sur les logiciels libres) a énoncé que «&nbsp;les administrations publiques qui développent et partagent leur logiciel ne doivent pas être perçues comme des pratiques anticoncurrentielles mais prises comme une opportunité pour les sociétés privées de construire et d’étendre leur offre de services.&nbsp;»

L’Open Source et les technologies ouvertes représentent donc une opportunité tout autant pour les collectivités que pour les entreprises privées. De nombreux exemples le prouvent aujourd’hui parmi lesquels les exemples d’Android (le système d’exploitation mobile de Google), Genivi (un consortium de constructeurs automobiles) ou encore OpenStack (une solution de *cloud computing* Open Source).



### Android, un succès commercial au service du consommateur

Android est un système d’exploitation mobile qui repose sur le noyau Linux. Racheté en 2005 par Google, il connaît un succès retentissant. Avec plus de 80% des parts de marché, cet outil, né d’une volonté farouchement collaborative et ouverte, a révolutionné nombre de nos comportements au quotidien. 
 
Android est le fruit de l’Innovation Ouverte. Au lancement du projet en 2008, la ligne était claire, il fallait qu’Android soit un produit que les utilisateurs puissent s’approprier, modifier, améliorer. Les codes sources ont été mis à la disposition des développeurs du monde entier et un appel à la communauté Open Source a été lancé. Il fallait enrichir Android et créer un écosystème d’applications et de petites entreprises qui gravitent autour afin de fournir un service riche et à l’image des besoins les plus proches des utilisateurs eux-mêmes. La stratégie d’Innovation Ouverte à visée écosystémique se révèle être aujourd’hui un pari gagné&nbsp;: Android c’est plus de deux millions d’applications disponibles, un large ensemble de bibliothèques Open Source, une déclinaison pour les tablettes, les téléviseurs (Android TV Input Framework), les voitures (Android Automotive) et dernièrement, les montres connectées (Android Wear).

Android est acteur de la transformation des usages. Alors qu’il s’agissait de son talon d’Achile à ses débuts, Android dispose aujourd’hui de la plus large bibliothèque d’applications pour ordiphones. Ces applications ont aujourd’hui complètement transformé le rapport des consommateurs aux objets que sont les téléphones fixes et mobiles et, bien au-delà, elles ont transformé leurs habitudes d’achat et de consommation des produits en ligne. Ainsi, rien qu’en France, le taux de pénétration de la téléphonie fixe est de 59,87% contre 94,89% pour la téléphonie mobile. Le nombre d’achats en ligne est en continuelle augmentation grâce à la multiplication des applications des commerçants, avec une tendance forte&nbsp;: le m-commerce, avec un chiffre d’affaires de 7 milliards d’euros en 2015 et une hausse de +38% pour le second trimestre 2016. L’acte d’achat se nomadise et il devient possible de consommer en tout lieu et à tout moment. Il en va de même pour la consommation des produits culturels, avec des applications mobiles qui fournissent du contenu disponible en ligne et non plus sur les appareils. On n’achète alors plus son CD ou sa chanson préférée en ligne, on les consomme à la demande sans jamais avoir la maîtrise du support, grâce à des formules d’abonnement. La dématérialisation, couplée à une disponibilité permanente au bout des doigts, transforme en profondeur nos habitudes. Cette facilité d’accès aux biens de consommation a été fondamentalement construite par les applications développées au sein des écosystèmes tels qu’Android -- le modèle de l’Open Source ayant été le moyen efficient pour atteindre la masse critique pour produire des marchés conséquents.


### Genivi

L’Open Source a aussi permis à des concurrents de dépasser des clivages anciens pour un futur commun, comme avec le projet Genivi. Genivi est un projet porté par des constructeurs automobiles qui ont souhaité répondre à l’informatisation croissante des équipements embarqués dans les véhicules. Contraction des noms de la ville de Genève et de IVI (*In-Vehicle Infotainment*), Genivi est aujourd’hui un des plus gros projets Open Source dans le domaine automobile. Basé sur le noyau Linux, il vise à créer un standard commun à l’ensemble des intervenants de la chaîne pour le développement des applications automobiles de demain. Initié en 2009 sous forme d’alliance entre BMW Group, Delphi, GM, Intel, Magneti-Marelli, PSA Peugeot Citroën, Visteon, et Wind River Systems, Genivi a été réorienté en 2012 pour se développer en stratégie ouverte et communautaire. Ce tournant décisif a été marqué par le dépôt des codes auprès de la Fondation Linux et une croissance du nombre des contributeurs. Genivi compte aujourd’hui plus de 160 entreprises et fournit une architecture logicielle mise à jour tous les 6 mois.

Ce projet mené en mode «&nbsp;Innovation Ouverte&nbsp;» repose sur la puissance d’un écosystème et l’adaptation aux usages des automobilistes de demain. Inspirés par la multiplication des applications sur les smartphones et la facilité de leur usage, les constructeurs imaginent des voitures où la prise de commande serait tout autant connectée, ergonomique et fluide. Or afin de garantir l’interopérabilité et la communication entre les smartphones, les montres connectées et le véhicule, il est indispensable de disposer d’un standard commun et partagé. La mise au point de ce standard est le coeur du projet Genivi.


### OpenStack, la puissance d’un écosystème

OpenStack est une solution de cloud computing Open Source développée en 2010 par Rackspace Hosting et la NASA pour leurs besoins propres. Depuis 2012, ce projet a pris son indépendance en passant sous la gestion d’une organisation à but non-lucratif, la Fondation OpenStack. La fondation a pour but de recueillir les dons, gérer la marque, la communauté ainsi que les entreprises partenaires du projet.

Un des succès les plus fulgurants de ces dernières années, OpenStack a su déployer une stratégie d’innovation largement ouverte sur son écosystème. En effet, avec des milliers de contributeurs et plus de 643 entreprises partenaires, ce projet présente toutes les caractéristiques d’une innovation collective et réussie&nbsp;:

   * une gouvernance claire et participative&nbsp;: des entreprises aux contributeurs particuliers, l’ensemble des parties prenantes du projet sont représentées à qualité égale,
   * un code source ouvert et disponible (licence Apache),
   * un rythme de mises à jour régulier (2 fois par an),
   * une communauté dynamique et ambassadrice (près de 65000 contributeurs dans 187 pays),
   * des outils de gestion de la contribution performants et stables.

# L’Innovation Ouverte concerne tous les domaines

On aurait tendance à penser que l’Innovation Ouverte est réservée aux projets qui relèvent du numérique ou encore des hautes technologies. On pense au laboratoire de R\&D de Palo Alto d’Intel, à IBM ou encore à Lucent -- des entreprises américaines qui sont à l’origine de la construction du concept d’Innovation Ouverte proposé en 2003 par Henri Chesbrough. 

Or, au travers des quelques lignes que nous venons de développer, on voit que l’Innovation Ouverte peut aussi émerger à l’initiative d’individus (Linux) ou d’un ensemble d’entreprises comme  dans le secteur plus traditionnel de l’automobile. Ceci est d’autant plus vrai aujourd’hui, à l’heure où l’accès aux outils qui permettent la coordination de telles stratégies est donné au plus grand nombre. On voit ainsi l’Innovation Ouverte se développer dans les domaines de la santé, de la mobilité, de l’agroalimentaire, des assurances ou encore du droit et de l’énergie.

## Approche par domaines d’activités


### Mobilité

Le déploiement du numérique dans l’écosystème de la mobilité modifie les équilibres du marché au profit de nouveaux entrants tels que les starts-up, en même temps qu’il favorise le développement de l’innovation d’usage. A contrario, les acteurs initialement présents sont contraints de repenser leur activité dans ce contexte de mutation en vue de la pérenniser. L’effet est bénéfique pour l’écosystème dans la mesure où il pousse les organisations à innover de manière continue et disruptive sans se reposer sur une position préétablie au sein du marché. Les lignes sont en train de bouger, comme on l’a vu récemment avec l’arrivée sur le marché de la société Uber. 


Dans ce cadre, il est nécessaire de créer une nouvelle culture de l’innovation en alliant public et privé dans un tiers lieu afin d’amorcer la transformation de l’écosystème de la mobilité. Il devient nécessaire d’accompagner les acteurs et les projets bâtis autour de cette idée. La Fabrique des Mobilités, en tant qu’accélérateur, soutient justement les entrepreneurs et les collectivités, ainsi que les porteurs de projets innovants dans ce domaine. Elle met à leur disposition un ensemble de ressources, notamment via son écosystème riche de partenaires et de structures parallèles existantes telles que les incubateurs. L’objectif est de favoriser un environnement de collaboration au service de communs puisque chaque projet choisi doit apporter des contributions aux plateformes ouvertes et ainsi, stimuler l’émergence des communautés.  

La Fabrique des Mobilités a ainsi soutenu le projet Catalogue porté par la société Transdev visant à créer un agrégateur de données ouvertes de mobilité. Le potentiel de ce projet à la fois Open Source et Open Data est énorme en termes d’innovation et permettra d’accélérer le développement des *smart cities*. De plus, il place l’utilisateur au centre des préoccupations, celui-ci pouvant accéder facilement et gratuitement à l’ensemble des données de mobilité, ce qui facilite le calcul d’itinéraire. La qualité des données est renforcée grâce à l’Open Data. Afin d’aboutir à ce résultat, le projet a rassemblé tout une communauté de développeurs, de producteurs de bases de données et d’usagers. Si les producteurs n’avaient pas accepté d’ouvrir leurs données en vue de les déposer gratuitement sur la plateforme et si l’ensemble des acteurs n’avaient pas été impliqués, le projet n’aurait pas pu voir le jour.


### Santé

L’Innovation Ouverte se développe désormais dans le domaine de la santé, avec d’une part le projet EchOpen qui met en avant le rôle de l’Open Hardware, et le projet Epidemium relatif à de l’Open Science.

   * *EchOpen et l’Open Hardware*&nbsp;: projet collaboratif et ouvert, il rassemble une communauté d’experts et de professionnels pluridisciplinaires autour de la conception d’«&nbsp;un outil révolutionnaire de sonde écho-stéthoscopique Open Source, à bas prix, connectée avec un mobile, une tablette ou un smartphone&nbsp;». Cette initiative modifie considérablement le rapport médecin / patient en introduisant de la dématérialisation dans la consultation. Il permet d’uniformiser les méthodes de diagnostic afin de rompre les inégalités et donne accès à davantage de personnes aux examens échographiques.
   * *Epidium et l’Open Science*&nbsp;: Epidemium est un «&nbsp;programme de recherche scientifique participatif et ouvert&nbsp;»[^epidemium] constitué autour d’une communauté d’experts spécialisés dans les sciences sociales, la data visualisation, les sciences de l’environnement, l’informatique, les mathématiques et la datascience. L’objectif est de faire avancer la recherche contre le cancer au travers des valeurs d’ouverture, de transdisciplinarité, de collaboration et d’indépendance. Dans ce cadre, la recherche s’organise autour de challenges (exemple&nbsp;: « Comprendre la répartition du cancer dans le temps et dans l’espace ») et de propositions de solutions de type data-analytiques en mettant à disposition des chercheurs un ensemble de ressources et notamment des jeux de données, un environnement de data analyse constitué d’un cluster Big Data (Teralab) ainsi que d’outils de data analyse. Les projets issus de chacun des challenges sont ensuite disponibles en ligne à tous. Epidium est soutenu par La Paillasse et par Roche Pharma France.


[^epidemium]: Voir le site [www.epidemium.cc](http://www.epidemium.cc/).


### Maritime

On observe ainsi le même phénomène dans le domaine maritime, avec l’exemple de Mercator Océan, PME innovante disposant d’une très forte expertise scientifique interne qui a été choisie comme délégataire du service *Marine* du programme européen d’observation de la terre, Copernicus. Si, dès le début, la société s’est reposée sur un réseau de partenaires européens pour opérer son portail de diffusion de données océanographiques, elle s’est aussi démarquée par une stratégie numérique innovante et ambitieuse qui lui a permis d’être préférée aux acteurs traditionnels de la Commission européenne. Elle s’est ainsi progressivement forgé une stratégie d’Innovation Ouverte ayant vocation à tirer le meilleur profit des technologies numériques existantes en s’inspirant des bonnes pratiques du secteur (des données et/ou métiers) et passant nécessairement par une plus grande écoute des tendances telle que l’*Open Data* –&nbsp;voire le *linked Open Data*&nbsp;– ou encore l’*innersourcing* (l’application des principes de l’Open Source aux projets et processus internes).

## Approche transversale

L’innovation invite au décloisonnement des secteurs d’activités et plus encore des domaines de compétences. Ainsi, une innovation peut s’inspirer de ce qui a été observé dans un autre domaine ou être le résultat d’un processus d’innovation transsectoriel (c’est l’idée de rapprocher deux domaines). L’écosystème pour s’enrichir doit piocher dans les autres domaines dans une vision transversale. L’Innovation Ouverte permet d’identifier les points de jonction entre les différentes disciplines, ce qui dynamise l’ensemble. 


### Une innovation conçue dans un domaine, transposable à un autre domaine

Les plateformes de crowdsourcing mettent en exergue ce phénomène dès lors qu’elles rendent possible la rencontre entre des publics qui ne se seraient probablement pas côtoyés dans d’autres conditions. En effet, le crowdsourcing consiste pour une entreprise à s’appuyer sur la créativité et le savoir-faire de la foule, plus particulièrement des internautes afin de les inclure dans le processus d’innovation. Ce mouvement participe à revisiter les concours et les défis lancés par les entreprises. L’ensemble des internautes peuvent concourir à ce dernier en proposant des solutions à partir d’une plateforme basée sur le crowdsourcing. Ainsi, il n’est pas rare que certains experts dans un domaine autre que celui de l’entreprise, proposent de transposer une solution qui existe dans un secteur donné, à celui de cette dernière. Malheureusement, la plupart des entreprises se réapproprient les résultats, ce qui empêche d’entrer dans un cercle vertueux où la réintroduction des contributions enrichies par les entreprises rend possible un accroissement global des savoirs.

### Le biomimétisme ou s’inspirer de la nature pour mieux innover demain

Le biomimétisme consiste à «&nbsp;s’inspirer des innovations du vivant pour apporter des solutions aux défis sociétaux actuels et futurs&nbsp;»[^biomime]. Plus précisément, il s’agit de tenir compte des formes présentes dans la nature, des procédés utilisés par le vivant et des écosystèmes dans leur diversité[^diversit]. Le CESE (Conseil économique social et environnemental) voit là la possibilité de «&nbsp;réconcilier la biosphère et la technosphère&nbsp;» puisqu’il s’agit d’associer biologie et innovation numérique en vue de favoriser le développement durable et de pallier l’uniformisation du numérique centrée sur les logiques économiques. C’est ainsi que les ailettes à l’extrémité des ailes d’un avion ont été inspirées des ailes du grand aigle des steppes, permettant de réduire la consommation d’hydrocarbure et les émissions de CO2. Dans le même esprit, l’électricité et les combustibles non-polluants ont été inspirés de la photosynthèse des végétaux. Ou encore, dans le but de réduire l’impact de l’industrie sur l’environnement, l’économie circulaire a créé de nouvelles ressources à partir des déchets industriels[^dechindu].

[^biomime]: Voir *Le soulèvement du Biomimétisme*, sur [medium.com](https://medium.com/we-are-biomers/le-soul%C3%A8vement-du-biomim%C3%A9tisme-2dad76c5171e#.lrdrsycqa).

[^diversit]: Voir l’avis du Conseil Économique, Social et Environnemental. Patricia Ricard, *Le biomimétisme&nbsp;: s’inspirer de la nature pour innover durablement*, sept. 2015 (J.O.), [doc. PDF sur www.lecese.fr](http://www.lecese.fr/sites/default/files/pdf/Rapports/2015/2015_23_biomimetisme.pdf).

[^dechindu]: Voir page «&nbsp;Le biomimétisme&nbsp;», sur [ceebios.com](http://ceebios.com/le-biomimetisme/).

En outre, le biomimétisme désigne à la fois une ingénierie et un processus «&nbsp;hybride de disciplines&nbsp;»[^hybride].

[^hybride]: Voir *Le soulèvement du Biomimétisme*, sur [medium.com](https://medium.com/we-are-biomers/le-soul%C3%A8vement-du-biomim%C3%A9tisme-2dad76c5171e#.lrdrsycqa). 

C’est ainsi que le Fab Lab Biome qui place le biomimétisme au coeur de l’innovation, rassemble une communauté d’acteurs diversifiés et spécialisés dans l’environnement, l’économie circulaire, les sciences, la participation citoyenne, l’urbanisme, l’architecture, l’agriculture, etc. Le biomimétisme repose ainsi sur la jonction d’approches provenant de différents domaines et mêlant des compétences diverses. À cet égard, Xavier Coadic, initiateur du projet Biome explique que le biomimétisme est pour lui «&nbsp;une opportunité de pratiquer mille disciplines&nbsp;»[^milledis]. Ce cadre favorable a permis de développer des objets utiles à partir de produits existants&nbsp;: un microscope DIY en détournant l’usage des smartphones, ou encore des abris d’urgence pour les réfugiés à partir de l’exosquelette du scorpion le dans le cadre du projet SCOdD[^scodd].

[^milledis]: Voir UPCAmpus&nbsp;: *Le Biome*. Sur [up-campus.org](http://up-campus.org/projets/voir/id/3813832e50ca89ac6b25c269e8cf4c13).

[^scodd]: Voir *Biomimicry et open Lab / Meet-up Paris 2015*, sur [http://lebiomefablab.wixsite.com](http://lebiomefablab.wixsite.com/lebiome/single-post/2015/02/12/Biomimicry-et-open-Lab-Meetup-Paris-2015).

De la même manière, le Ceebios (Centre Européen d’Excellence en Biomimétisme de Senlis), centre d’innovation technologique, a développé de nombreuses activités complémentaires mêlant recherche et industrie afin de favoriser la coopération entre les différents acteurs.

Face aux potentialités qu’il représente, les entreprises se sont saisies du biomimétisme et des opportunités qui l’accompagnent. C’est ainsi qu’a été lancée la start-up Enzyme\&Co, constituée d’une équipe multidisciplinaire associant experts en design et en ingénierie. Elle propose à la fois des projets de design et de recherche dont l’objectif est de «&nbsp;formuler de nouveaux scénarios bio-inspirés&nbsp;», et ce en fonction des différentes thématiques mises en avant par les partenaires (mobilité, textile, santé, etc.). La société Enzyme&Co a par exemple créé la bouilloire Nautile, «&nbsp;électrique et à combustion en argile, fabriquée à partir de technologies de l’impression 3D&nbsp;» dont l’impact environnemental est réduit[^enviroredui].

[^enviroredui]: *Nautile, bouilloire bio-inspirée*, description sur le blog [www.enzymeandco.com/blog](\url{http://www.enzymeandco.com/blog/portfolio/nautile/).


### Blockchain, réplicable au-delà du secteur bancaire

La Blockchain est la réunion d’un ensemble de technologies assurant l’inaltérabilité et la vérifiabilité des transactions au sein d’un registre partagé entre tous. Par son fonctionnement décentralisé et l’emprunt à de nombreuses technologies et pratiques de l’Open Source, la Blockchain est un projet éminemment communautaire qui tire toute sa force et son succès de l’engouement qu’il a su générer. La logique d’Open Source induit immédiatement une réflexion en termes de puissance communautaire, les millions d’utilisateurs de la Blockchain étant sans aucun doute sa plus grande richesse. Elle attire aujourd’hui l’attention d’énormément de grands comptes (les banques et assurances en premier lieu) qui fluctuent entre des initiatives individuelles et collectives.

À l’instar d’Internet qui ne s’est développé qu’à partir de protocoles Open Source et décentralisés, le succès de la Blockchain repose uniquement sur des technologies libres et Open Source. Le protocole Blockchain d’origine et toutes ses implémentations —&nbsp;dont Ethereum&nbsp;— sont eux-mêmes sous licence GNU General Public License&nbsp;v2[^gnugpl]. Il est même probable que cette dimension juridique de la Blockchain soit l’une des composantes essentielles de son succès —&nbsp;ce qui amène à penser que tout autre choix aurait grandement limité l’adoption de la technologie et que des projets de Blockchain fermés (on parle généralement de *permissioned blockchain*) mésestiment certainement l’une des caractéristiques non techniques les plus fondamentales (les enjeux de sécurité et d’évaluation par les pairs étant d’autant plus importants que les opérations sont stratégiques). Ce modèle permet de faire évoluer les technologies et privilégie a priori un code de qualité compte tenu de la concurrence entre les projets et de l’importance de la sécurité parmi les critères distinguant chaque projet.

[^gnugpl]: La licence GNU General Public License est une licence à réciprocité (le logiciel et toute modification apportée doivent rester soumis à cette même licence) publiée par la *Free Software Foundation* et le *Software Freedom Law Center*. Elle garantit à tous les utilisateurs et contributeurs que le code et ses dérivées resteront soumis à ce même régime inclusif.

Concernant le développement même des protocoles sous-jacents à la Blockchain, certaines règles se dessinent aujourd’hui afin de permettre un modèle de gouvernance compatible avec l’organisation horizontale qui caractérise les principales implémentations de la Blockchain.

Avec la Blockchain, le pouvoir d’intervention s’est déplacé des autorités centralisées (tels que les opérateurs en lignes ou les autorités judiciaires) vers une communauté décentralisée de pairs, qui ont la possibilité de se coordonner afin de changer les règles du jeu. Son potentiel disruptif est donc particulièrement important là où une désintermédiation peut être introduite.


### Impression 3D

Un bon exemple aussi en matière d’innovation sociétale et de disruptivité des modèles ouverts&nbsp;: l’impression 3D. Alors que ces technologies existent depuis de nombreuses années, le terme de certains des brevets associés a entraîné une réelle extension du phénomène auprès du grand public (avec des modèles d’imprimantes 3D comme la reprap qui est en capacité de s’imprimer elle-même pourvu que l’on ait la matière première). De plus en plus d’écoles, d’universités, mais aussi d’entreprises offrent la mise à disposition de telles machines pour permettre à chacun d’expérimenter des idées et des projets à moindre coût. Plus encore, cette Innovation Ouverte ouvre le champ à un changement de paradigme aux effets non encore maîtrisés&nbsp;: l’utilisateur final étant en capacité de réaliser des opérations auparavant réservées à la seule industrie (l’impression de montures de lunette, de chaussures, pièces de rechange, etc.), la propriété intellectuelle est frontalement remise en cause et de nouveaux modèles économiques vont certainement s’en saisir (vendant des droits ou des plans ensuite réalisés chez le client final directement, sans aucun autre besoin de logistique).


# Les valeurs soutenant cette dynamique

Les projets et les initiatives déployées dans une logique d’Innovation Ouverte obéissent à des valeurs communes. Ces dernières permettent de renforcer la confiance entre les acteurs de l’écosystème de sorte à consolider durablement les liens entre les parties prenantes. 

Parmi ces valeurs, on distingue la transparence, la collaboration et la mutualisation, l’ouverture ainsi que la décentralisation.

La confiance dans «&nbsp;la capacité créative de chaque partie&nbsp;» apparaît quant à elle comme un véritable ciment pour les écosystèmes[^ciment].

[^ciment]: Voir la page Wiki&nbsp;: *Histoire de La Fabrique des Mobilités*, [wiki.lafabriquedesmobilites.fr](http://wiki.lafabriquedesmobilites.fr/wiki/Histoire_de_La_Fabrique_des_Mobilit%C3%A9s).

## La transparence

La transparence est une des principales conditions de la réussite d’une stratégie d’Innovation Ouverte. Celle-ci porte sur des niveaux et des objets différents du projet. Il s’agit dans un premier temps de garantir la transparence sur les objectifs de ce dernier. A ce titre, il est important&nbsp;:

   * que l’ensemble des contributeurs sollicités s’alignent sur un objectif commun, même si leurs attentes vis-à-vis de ce projet peuvent diverger&nbsp;;
   * d’avoir une vue d’ensemble claire et précise sur les modes de gouvernance et de gestion des potentiels conflits. Les procédures de contribution,leur acceptation et le rôle de chacun doivent être transparents afin d’éviter les malentendus, le travail en doublons et la mauvaise allocation des ressources&nbsp;; 
   * de  mettre à disposition toutes les informations liées au projet en lui-même&nbsp;: documentations, codes, accès etc. Toutes ces précautions vont permettre à tout un chacun d’avoir accès au même niveau de connaissances, quelle que soit son ancienneté dans le projet. Cela permet de diminuer les barrières à l’entrée et de gommer les effets de pouvoir. Ainsi nul n’est plus sachant que l’autre dans les processus mis en place pour le développement. De plus, diminuer les asymétries d’informations participe à l’instauration d’une relation de confiance entre les acteurs d’un même projet. Afin d’inclure correctement des partenaires, il faut donc pouvoir justifier d’une transparence irréprochable envers eux, notamment sur les conditions de participation au projet.

Notre propos s’appuie notamment sur le paradigme de la Document Foundation (la fondation qui développe le projet LibreOffice, suite bureautique Open Source) qui a su créer un écosystème basé sur la transparence autour de LibreOffice. Si tout le code est disponible sur les dépôts de la fondation, il en est de même pour l’ensemble de sa documentation ( la documentation du produit ainsi que toute la documentation qui concerne les processus de développement en eux-mêmes&nbsp;: guide pour écrire le code, les tests, etc.) et des tests qui sont effectués sur le produit (qu’ils soient manuels ou automatisés). Mais cela va plus loin encore puisque la fondation a mis la transparence au cœur de sa gouvernance. Tout le fonctionnement de la fondation est donc mis à disposition du public. Ainsi, son rapport d’activité est publié tous les ans, les livres de compte sont mis à disposition mensuellement, les comptes-rendus des réunions du conseil d’administration sont publiés toutes les semaines, ainsi que leur enregistrement audio. En outre, la fondation ne vit que par les dons des utilisateurs des produits qu’elle héberge, il est donc important que ces donneurs sachent à quoi est employé l’argent et puissent avoir un aperçu du fonctionnement et de la santé de la fondation. Cette transparence permet de construire une relation de confiance avec les donneurs.

## La collaboration et la mutualisation

Une stratégie d’Innovation Ouverte se déploie par définition en collaboration avec des entreprises partenaires, une communauté de contributeurs et même avec des concurrents. Plus l’ouverture vers l’extérieur est grande, plus la richesse des compétences sera grande et variée. La construction de l’écosystème où va se déployer le projet constitue un facteur de succès important, car la réussite d’une innovation ne tient parfois qu’à une relation près. Il faut être prêt à envisager la collaboration avec des acteurs très variés, dans des domaines qui peuvent parfois être assez éloignés du coeur de métier initial (exemple...) et même avec des concurrents. En effet, malgré une rivalité de fait sur le marché, on constate que les concurrents partagent souvent bien plus qu’ils ne le soupçonnent&nbsp;: une vision du marché commune, des savoir-faire spécialisés mais complémentaires, des ressources humaines et parfois même des intérêts financiers partagés. Cette «&nbsp;coopétition&nbsp;» (contraction des termes «&nbsp;coopération&nbsp;» et «&nbsp;compétition&nbsp;») a été observée dans des domaines aussi variés que l’équipement télévisuel (collaboration entre Sony et Samsung pour les écrans LCD), l’aérospatial (collaboration entre Astrium et Thales Alenia Space), l’industrie pharmaceutique (collaboration entre Sanofi et BMS) ou encore l’agro-alimentaire (collaboration entre Pepsi et Coca-Cola).

Le projet Genivi et le système d’exploitation Android sont également nés à partir d’une stratégie d’Innovation Ouverte coopétitive. En effet, pour Genivi, c’est un groupement de constructeurs concurrents qui est à l’origine de l’initiative. Pour Android, la constitution d’une alliance entre divers constructeurs d’ordiphones, des opérateurs et des distributeurs a été un élément clé de succès. Espace de partage de bonnes pratiques, centre de discussions stratégiques et *patent pool*, cette alliance a réuni autour de la même table des fabricants concurrents tels que LG, HTC, Huawei, Samsung ou encore Sony. Tous travaillant au même projet, celui du système d’exploitation Android.

Pour les projets d’Innovation Ouverte qui intègrent les utilisateurs comme contributeurs à part entière (OpenStack, Procter&Gamble), il y a une volonté de responsabilisation vis-à-vis de ces acteurs qui, en définitive, participent tout autant à l’élaboration de l’innovation. Ils peuvent être une source d’idées et de propositions nouvelles mais aussi des testeurs qui sanctionnent un résultat. Ils peuvent là aussi intervenir à différentes étapes du développement du projet. Ils participent également au développement de l’écosystème en faisant développer ou corriger des fonctionnalités par des entreprises spécialisées qui reversent alors le code au projet Open Source. Ces allers-retours entre l’entreprise et ses contributeurs permettent d’enclencher un cercle vertueux.

Cependant, celui-ci n’est maintenu et bénéfique à tous que si l’ensemble des participants respecte les principes de transparence et de mutualisation. La mise en place d’une gouvernance équitable, dans laquelle chacun des partenaires accepte de jouer un rôle similaire, est donc essentielle. Elle vise à minimiser le risque que certains des partenaires tirent avantage de leur puissance pour biaiser la coopération en leur faveur. Tel est par exemple le cas de Google avec Android. Lorsque ce système fut lancé en 2007, le marché des équipements mobiles était dominé par des systèmes d’exploitation fermés. La crainte de Google était qu’Apple accapare ce marché, en proposant des services intégrés concurrents à ceux de Google. Celui-ci ne pourrait alors que dépérir, privé d’un gisement de données et de revenus qui allaient supplanter ceux issus des équipements fixes, non seulement en volume, mais surtout en valeur, du fait des informations pouvant être extraites de la géolocalisation. Google lança alors l’*Android Open Source Project* (AOSP) selon un modèle ouvert, afin de bénéficier de la puissance de tout un écosystème, incluant de nombreux constructeurs opposés à la domination d’Apple et ravis de bénéficier d’une plateforme au développement mutualisé et bénéficiant d’un accès intégré aux services de Google, déjà très populaires. Or, dès avant qu’Android ne représente plus de 85% du marché, Google a commencé à modifier sa stratégie, en faisant porter ses efforts de développement non plus sur les briques ouvertes d’origine du projet AOSP, mais sur des briques fermées concurrentes, à l’ergonomie plus achevée. L’objectif fut alors d’inciter les développeurs à utiliser préférentiellement celles-ci, fragmentant l’écosystème applicatif de l’AOSP, dans le but de sécuriser l’alimentation des entrepôts de données de Google [^entrepotgoo]. En réaction, et suite à l’entrée frontale de Google sur le marché des ordiphones avec sa gamme Pixel, les anciens partenaires de l’AOSP ont réorganisé leur stratégie d’alliances afin de fournir des systèmes moins liés à celui-ci, au point que les différents *forks* d’Android représentent près de 20% de l’écosystème. Cet exemple illustre bien que la coopétition doit être abordée sans angélisme, et que les différents partenaires doivent toujours s’évaluer.

 [^entrepotgoo]: Ron Amadeo, «&nbsp;Google’s iron grip on Android: Controlling open source by any means necessary&nbsp;», ArsTechnica, 2013. [http://arstechnica.com](http://arstechnica.com/gadgets/2013/10/googles-iron-grip-on-android-controlling-open-source-by-any-means-necessary/).

## L’ouverture par l’inclusion

L’inclusion suppose d’ouvrir largement la collaboration et la mutualisation à l’ensemble des acteurs de l’écosystème. Pour que le projet s’adresse à un large panel de partenaires, les conditions de participation au projet ne doivent pas être restrictives. Les conditions de nature à exclure peuvent se matérialiser par des barrières économiques telles qu’un coût d’adhésion ou des frais à engager trop importants, ou bien des conditions de statut, de taille ou encore d’origine du contributeur. Les acteurs doivent donc s’assurer que l’accès à l’écosystème qu’ils sont en train de construire est clair, lisible et ouvert au plus grand nombre. Cela doit se prolonger par la suite dans les différentes étapes de développement du projet. Afin d’éviter l’exclusion de certains de l’effort collectif, il est important de rester transparent et de tout mettre en œuvre pour que l’ensemble des participants aient accès aux mêmes informations et aux mêmes données.

Ainsi, une logique beaucoup plus constructive apparaît lorsque plusieurs acteurs, concurrents ou non, décident de coordonner ensemble les efforts nécessaires à l’émergence d’une innovation. Ayant contribué à sa survenance, ils seront d’autant plus aptes à en tirer les bénéfices et n’ayant pas d’autres objectifs que de continuer à innover, ils poursuivront dans cette voie —&nbsp;faisant évoluer la société en même temps que leur entreprise.

Une telle logique collective est par principe très inclusive puisque chaque nouvel acteur est un contributeur qui participera à ce titre à l’effort de mutualisation.

Pour rassembler ainsi un maximum d’acteurs d’un écosystème autour de projets communs, avec pour objectif d’être le plus inclusif possible, il est nécessaire de mettre en place un modèle ouvert qui garantit un bénéfice à chacun, malgré des intérêts qui peuvent parfois se trouver divergents. À cet égard, s’inscrire dans une dynamique collaborative permet de faire valoir les intérêts de chacun dans un esprit de conciliation, en même temps que de dépasser ces derniers en prenant conscience de l’intérêt collectif représenté par l’écosystème.

### La collaboration de tous, un impératif face aux changements

#### Open Law

Se rassembler pour accompagner les mutations de l’écosystème du droit, les acteurs du droit l’ont bien compris, comme le démontre le projet Open Law&nbsp;: le droit ouvert. À la fois territoire d’innovation collaborative et laboratoire pour l’expérimentation de nouveaux services, Open Law rassemble ainsi «&nbsp;acteurs de la Legal Tech&nbsp;» et professionnels du droit autour de programmes de co-création visant à explorer collectivement l’ensemble des opportunités offertes par le numérique pour la transformation et la modernisation du monde du droit. 

Dès lors que l’innovation se veut inclusive et que la majeure partie de l’écosystème y prend part, il est possible d’entrer dans une démarche systémique, ce qui est le cas avec Open Law. Tous les acteurs (éditeurs, avocats, notaires, secteur public…) sont représentés, car ils ont très vite compris l’intérêt d’y participer afin d’accompagner le changement, plutôt que d’en être exclus. En effet, ils sont partis du constat que le système à terme se modifiera et ont donc pris conscience de la nécessité de s’y associer. À ce titre, Open Law met en lumière un phénomène émergent qui est celui de la mutualisation verticale au sein de certains métiers. Inciter de cette manière les acteurs du droit à la co-construction permet d’avoir une transition progressive, respectueuse des valeurs clés de la déontologie et des professions.

Si le projet rencontre une large adhésion, c’est que l’association a su créer les conditions propices à la collaboration. Tout d’abord, elle est rapidement apparue comme un tiers lieu neutre instaurant un climat de confiance entre les parties prenantes. Ensuite, elle a adopté une approche et une méthodologie ouvertes permettant d’inclure l’ensemble des parties prenantes. 

À partir de ce rapprochement, un certain nombre de communs ont été définis pour répondre à la question suivante&nbsp;: «&nbsp;Quelles sont les ressources nécessaires pour que chacun puisse innover&nbsp;?&nbsp;». Des initiatives similaires émergent dans des domaines aussi variés que la santé, l’énergie, la mobilité etc. Les potentialités sont colossales. Des référentiels communs émergent et permettent ainsi aux acteurs d’interagir. Tous les acteurs ont intérêt à jouer collectif. 


#### Partenariat pour l’intelligence artificielle au bénéfice des citoyens et de la société

L’innovation tend à rattraper l’Homme, on le voit avec l’intelligence artificielle. Cette dernière modifie les lignes actuellement existantes et présente un certain nombre d’enjeux dont l’ampleur est telle pour la société que la collaboration entre acteurs du secteur et au-delà est apparue comme un impératif. Dans ce cadre, le partenariat vise à soutenir la recherche et à formuler des recommandations sur les bonnes pratiques à adopter en lien notamment avec l’inclusion, la transparence, l’interopérabilité, l’éthique, le respect de la vie privée. L’objectif est également celui de sensibiliser les publics à l’intelligence artificielle en mettant en avant ses bénéfices au travers de l’expertise proposée et de l’information. Pour ce faire, le «&nbsp;Partenariat pour l’intelligence artificielle au bénéfice des citoyens et de la société&nbsp;» a mis en place une plateforme ouverte répertoriant les engagements pris par les différentes parties prenantes et sur laquelle il sera possible d’échanger sur le sujet. Il a adopté une logique transdisciplinaire en permettant la participation d’experts de différents domaines afin que ces derniers proposent des orientations relatives à l’intelligence artificielle, et une logique collaborative en y associant des développeurs, des utilisateurs et des industriels.

### Les bénéfices de l’inclusion sur l’innovation


#### Accroissement du progrès technique par le collectif

L’innovation peut être entendue à la fois comme «&nbsp;un processus qui mène une idée à un produit, un service, un procédé, un modèle ou une organisation innovante (...) et le résultat (...) de ce même processus&nbsp;». De là, il ressort que plus les acteurs de l’écosystème sont nombreux à se mobiliser autour d’un projet commun lors du processus d’innovation et plus l’innovation en résultant sera source de progrès, car basée sur la complémentarité ainsi que la diversité des compétences de chacun. Cette démarche facilite le transfert de compétences au travers des différents échanges de savoirs occasionnés par les méthodes de travail basés sur le collectif. Cela renforce également la capacité des acteurs à porter leurs projets à l’échelle de l’écosystème et facilite l’émergence des innovations de rupture. 

#### Fédération des acteurs pour générer une approche systémique

Plus le caractère inclusif d’un projet est marqué et plus il aura tendance à fédérer dans une logique systémique. Il s’agit d’un effet de levier de participation. Ainsi, même lorsque l’approche systémique n’est pas souhaitée par les acteurs en place, cette dernière s’avère être suffisamment forte pour entraîner *in fine* les acteurs qui lui étaient auparavant rétifs. Il devient alors plus facile de leur faire comprendre la nécessité de travailler ensemble.

## La décentralisation

Dans le cadre de l’Innovation Ouverte, l’écosystème est envisagé de manière décentralisé avec une architecture en réseaux. Tel est le cas d’internet, réseau structuré sous forme de toile, ne disposant d’aucun point central et reposant sur le principe de symétrie selon lequel chaque ordinateur est à la fois serveur et client. Ce faisant, les noeuds du réseau global permettent de faire communiquer les sous-réseaux en les interconnectant. De cette manière, internet a fait valoir son caractère disruptif puisqu’auparavant, tous les réseaux étaient centrés et les terminaux passifs. L’exemple le plus emblématique est le minitel composé d’un réseau en étoile qui obligeait chaque utilisateur à interroger le serveur central. Il en résulte que sans la décentralisation, internet ne serait pas ce qu’il est, et que la majorité de son infrastructure est conçue sur la base d’Open Source.

Il n’y a néanmoins pas de réelle décentralisation sans interopérabilité et la structure d’internet repose à ce titre beaucoup sur l’interopérabilité[^interop]. Cette dernière désigne «&nbsp;la capacité que possède un produit ou un système, dont les interfaces sont intégralement connues, à fonctionner avec d’autres produits ou systèmes existants ou futurs et ce sans restriction d’accès ou de mise en oeuvre&nbsp;», selon l’Association Francophone des Utilisateurs de Logiciels Libres (AFUL). Ainsi, toute personne souhaitant créer un site internet n’a pas à demander d’autorisation. À des fins d’interopérabilité, internet et ses applications (courrier électronique, web et échanges de fichiers par FTP) sont basés sur des normes[^normesbas], standard, de formats ouverts et de protocole. 

[^interop]: Attention néanmoins, à ne pas confondre interopérabilité avec compatibilité. La compatibilité renvoie au cas où plusieurs technologies sont seulement en mesure de communiquer. L’interopérabilité va plus loin car tant les informations techniques que juridiques nécessaires pour l’implémentation d’une technologie dans d’autres produits, doivent être disponibles.

[^normesbas]: Une norme est «&nbsp;un document établi par consensus, approuvé par un organisme reconnu, qui fournit, pour des usages communs et répétés, des règles, des lignes directrices ou des caractéristiques, pour des activités ou leurs résultats, garantissant un niveau d’ordre optimal dans un contexte reconnu&nbsp;» (Norme NF EN 45020 *Vocabulaire de la normalisation*).

Ces derniers permettent de mettre en place les conditions juridiques et techniques afin qu’il n’y ait pas de barrière à l’entrée et d’empêcher une entreprise d’être dominante sur le réseau internet. Elles suscitent ainsi la confiance du consommateur et permettent une réduction des prix. ODF est un format présentant une certaine neutralité, ce qui favorise la concurrence et de la même manière l’innovation. L’interopérabilité est donc bénéfique pour l’avancée technologique en ce qu’elle permet l’amélioration des technologies existantes, et la création de nouvelles solutions à partir de celles-ci. 

En outre, internet est également considéré comme ouvert car «&nbsp;sans logiciels libres pas d’internet, et sans internet pas de logiciels libres&nbsp;»[^bbayart]. D’une part internet s’est largement déployé sur la base du noyau Linux Kernel. D’autre part les contributions se font exclusivement sur internet. En ce sens, les logiciels libres sont interopérables dans la mesure où les spécifications sont disponibles et notamment les informations d’interopérabilité. Avec internet, on retrouve la même logique que pour les logiciels libres, à savoir que plus il y a d’utilisateurs sur le réseau et plus ce dernier s’enrichit, ce qui renforce son attractivité et permet d’attirer par là même de nouveaux utilisateurs. 

[^bbayart]: Benjamin Bayart, *Internet libre ou Minitel 2.0*, conférence, Amiens, 2007. Voir sur [fdn.fr](https://www.fdn.fr/actions/confs/internet-libre-ou-minitel-2-0/).

L’internet des objets est un nouveau champ des possibles pour l’interopérabilité. À cet égard, l’IEEE (Institute of Electrical and Electronics Engineers) a lancé le groupe de travail P2413 afin de créer un cadre d’interopérabilité adapté aux objets connectés. L’objectif est d’aboutir à une norme internationale commune en vue que l’internet des objets dispose d’une architecture standard alors que de nombreux industriels ont développé des spécifications propres qui parfois se chevauchent. L’ambition d’un tel projet est que l’internet des objets soit le «&nbsp;pilier de la prochaine révolution industrielle&nbsp;» en favorisant la coopération. La croissance du marché de l’internet des objets s’en trouvera accélérée.

Néanmoins, internet et sa couche applicative sont en train de tendre de plus en plus vers le modèle du minitel. Un deuxième mouvement est en train d’apparaître qui est celui de la redécentralisation notamment de la couche applicative et plus particulièrement du Web. En effet, on observe une réappropriation des communs et un recentrage de l’écosystème autour des GAFA. S’agissant des courriels, Facebook tend à proposer une offre parallèle dès lors que massivement utilisée, mais aussi de manière complètement centralisée.

Dans une démarche d’écosystème, il est nécessaire d’imposer aux acteurs l’interopérabilité de leurs solutions, indispensable au bon fonctionnement des projets d’Innovation Ouverte. Pour ce faire, l’interopérabilité doit être garantie à tous les niveaux&nbsp;: du logiciel, du matériel, et des jeux de données utilisés, autant matériellement que juridiquement. Ainsi plusieurs acteurs qui souhaitent concourir à l’élaboration d’une spécification commune en vue de favoriser l’interopérabilité autour de leurs produits se tournent généralement vers des logiques de standardisation auprès des organismes compétents —&nbsp;avec toutes les garanties offertes par lesdits organismes, notamment en termes de propriété intellectuelle&nbsp;— ou *de facto*. Solution intermédiaire ou transitoire lorsque réalisée en amont d’une standardisation ou entre deux versions de la spécification, il est aussi possible de recourir à des logiques contractuelles similaires aux modèles ouverts. Réalisée à la demande de la Commission Européene, INTILA est un modèle de licence adaptable et configurable qui est proposé comme canevas pour concéder sous licences des Informations d’Interopérabilité (ce qui comprend APIs, formats et protocoles) afin de permettre aux bénéficiaires de concevoir et exploiter des produits interopérables. Il devrait permettre aux concédants et licenciés de réduire les coûts de transaction grâce à la sélection et l’utilisation d’un ensemble de termes contractuels communément acceptés et considérés comme sûrs par une large communauté. Il permet ainsi aux entreprises de définir un cadre clair, non ambigu et juridiquement solide, favorisant le développement de produits interopérables avec leurs technologies. Parmi les acteurs qui réfléchissent de la sorte aux interfaces liées aux projets développés, citons notamment le projet Genivi dans le domaine de l’infodivertissement au sein des véhicules ou encore la technologie Calypso soutenue par Calypso Networks Association dans le domaine du transfert d’
information sans contact (puces Navigo, carte SNCF, etc.)[^bjenplaurent].

[^bjenplaurent]: Benjamin Jean, Philippe Laurent, *Model contracts for licensing interoperability information. The INTILA licence template (Interoperability Information Licence Agreement)&nbsp;: final report*, European Commission, 2013. Voir sur [bookshop.europa.eu](http://bookshop.europa.eu/en/model-contracts-for-licensing-interoperability-information-pbKK0414809/?CatalogCategoryID=6hcKABstX5QAAAEjs5AY4e5L).




\part{Transformer une organisation pour pleinement tirer profit de l’innovation ouverte}

Les marchés sont amenés à devenir transversaux en raison des échanges d’idées au sein des écosystèmes et du renforcement de la proximité des parties prenantes. Dans ce contexte, l’entreprise doit chercher à se positionner dans son environnement. En effet, participer à une innovation au sein de son écosystème  en s’appuyant notamment sur l’Open Source, l’Open Data et l’Open Hardware est le seul moyen pour l’entreprise de tirer pleinement les bénéfices de la diversité de son écosystème. Ces modèles vont ainsi diffuser leurs valeurs pour accélérer l’innovation, la catalyser et lui donner le cap nécessaire pour permettre à différents acteurs de travailler ensemble en toute confiance et en créer des opportunités pour les nouveaux entrants sur le marché. Des acteurs tels que Twitter, Facebook, Google, la NASA, le CERN, sont autant d’exemples d’organisations qui ont bâti leur infrastructure grâce à la maturité de l’Open Source mais qui sont aussi à l’origine de projets communautaires visant à mutualiser avec leurs pairs des ressources directement ou indirectement nécessaires à la production de leur innovation. À cet égard, interagir avec cet écosystème n’est pas sans conséquence organisationnelle pour l’entreprise et suppose d’adapter les méthodes de management ainsi que les processus de développement des innovations en interne. Pour ce faire, il est nécessaire de repenser son modèle économique en décloisonnant les frontières internes et externes de l’entreprise en vue d’avoir une approche globale des acteurs de l’écosystème et afin de valoriser au mieux les compétences de chacun.

# Ouvrir l’entreprise à l’écosystème

L’entreprise doit chercher à se tourner systématiquement vers son écosystème et de la même manière s’ouvrir à celui-ci. L’Open Data, l’Open Source et l’Open Hardware apportent une garantie de pérennité et de non-appropriation qui rassure les différents acteurs économiques et les encourage à partager leur propre contribution au profit d’autres acteurs qui apportent à leur tour maintenance et évolutions au profit de tous. Même si des outils juridiques et techniques accompagnent ces usages, c’est surtout une évolution culturelle qui doit être conduite au sein de l’entreprise pour imaginer tous les bénéfices qu’elle pourrait tirer d’une telle mutualisation. En effet, «&nbsp;ceci suppose pour la filière d’accepter de ne plus tout maîtriser et de se mettre au service des autres acteurs déjà identifiés ou non. De créer les conditions de réussite, de développer une nouvelle culture de l’innovation et d’ouvrir ses ressources, sans savoir à l’avance ce qui adviendra. De co-construire ensemble un nouveau dispositif d’émergence et d’accélération de l’innovation&nbsp;»[^fabrique].

[^fabrique]: Voir la page Wiki&nbsp;: *Histoire de La Fabrique des Mobilités*, [wiki.lafabriquedesmobilites.fr](http://wiki.lafabriquedesmobilites.fr/wiki/Histoire_de_La_Fabrique_des_Mobilit%C3%A9s).


## Changer la culture de l’entreprise

Accepter d’être transparent, partager ses données, travailler avec de nombreux partenaires et ne pas hésiter à en inclure de nouveaux, qui viendraient de secteurs différents etc., tout ceci représente bien plus qu’une simple volonté. Faire de l’Innovation Ouverte suppose une décision ferme et des moyens adéquats qui vont non seulement permettre d’innover à plusieurs, mais bien plus encore, de changer la culture de l’entreprise afin que l’Innovation Ouverte ne soit pas un simple coup d’essai mais véritablement une stratégie ancrée dans les valeurs de l’entreprise.

### Dépasser le syndrome du Not Invented Here (NIH)

Le syndrome du Not Invented Here correspond au fait de rejeter toute innovation provenant de l’extérieur de l’entreprise. En effet, pour diverses raisons, certains acteurs préfèrent "réinventer la roue" plutôt que d’utiliser l’existant. Ce fut la politique d’un bon nombre de grandes entreprises durant de longues années, notamment dans le domaine du logiciel, où les entreprises préféraient développer de zéro des outils qui pourtant étaient disponibles par ailleurs.

De nombreuses raisons sont évoquées pour justifier cette stratégie&nbsp;:

   * maîtriser la fabrication pour contrôler la qualité,
   * développer sa propre solution et en tirer une rente,
   * réutiliser des solutions existantes, notamment dans le domaine du logiciel, peut exposer à des risques de violation de brevet (spécifique aux Etats-Unis).

L’esprit de l’Innovation Ouverte rejette justement ce syndrome et prône au contraire la réutilisation, l’ouverture vers l’existant et son intégration dans l’entreprise. Ce qui n’est pas développé en interne n’est pas forcement dangereux ou moins bon, bien au contraire. Il s’agit de ne plus penser en termes de protection mais plutôt de gestion des biens, en particulier des biens immatériels. L’apprentissage et l’enrichissement mutuel poussent à sortir des logiques strictes de la réappropriation pour envisager l’innovation comme un ensemble de flux entrants et sortants de l’entreprise. L’étape ultime consiste d’ailleurs à réinjecter systématiquement ce qui est produit dans l’écosystème pour perpétuer le cycle d’apprentissage.

### Chercher les compétences en dehors de l’entreprise 

Selon, Bill Joy, co-fondateur de Sun Microsystems «&nbsp;No matter who, you are, most of the smartest people work for someone else&nbsp;» (dite *Joy’s Law*)[^joylawforbes]. Il amorce ainsi l’idée selon laquelle les meilleures compétences sont en dehors de l’organisation. Dans le prolongement, Jim Whitehurst, CEO de Red Hat a précisé que&nbsp;: «&nbsp;If you really want innovation to happen, you almost have to think about it as an ecosystem. A lot of companies think that the way to be more innovative is to put a group of creative people together. But your most creative ideas are going to come from people on the front lines who see a different way of doing the jobs they do every day. You have to create vehicles for those ideas to be heard.&nbsp;»

[^joylawforbes]: Voir page *Joy’s law (management)* sur [en.wikipedia.org](https://en.wikipedia.org/wiki/Joy’s_law_%28management%29).

Partant de ce constat, l’organisation ne doit plus chercher à recruter les meilleurs talents mais à travailler avec eux par le développement des réseaux. Il ne s’agit plus de s’attacher à développer un savoir interne mais de s’interroger avec qui faire le projet. Ceci déplace le centre névralgique en termes de recherche de compétences et a un impact direct sur la politique de ressources humaines. En effet, dans ce cadre, l’idée est de capitaliser sur les ressources externes en plus des ressources internes et de ne plus penser en termes d’organisation mais de projet. Dès lors, la stratégie consistant pour l’organisation à trouver les compétences les plus adaptées à un projet déterminé, bascule des ressources humaines vers la recherche d’investissements, de partenariats, de communautés etc.

L’organisation doit alors adopter une vision plurielle et globale afin de prendre conscience de l’environnement qui l’entoure ainsi que de la diversité de ses acteurs. Une bonne connaissance de son écosystème lui permettra de détecter au mieux les complémentarités qui existent tant en termes d’expertise, que vis-à-vis d’autres projets similaires ou transposables.

   * S’agissant de l’expertise&nbsp;: l’écosystème de l’entreprise ne doit plus être entendu comme se limitant aux acteurs de son secteur d’activités ou à la relation fournisseur / client. Plus largement, il inclut l’ensemble des parties prenantes à savoir&nbsp;: les clients / les utilisateurs, les autres organisations du même secteur, les concurrents, les communautés, le secteur public. Concernant plus particulièrement les utilisateurs, ces derniers sont les plus à même de remonter les problèmes ou les améliorations concernant un produit. De même, la diversité de la population utilisatrice permet d’avoir une assurance qualité sur un panel de fonctionnalités que l’entreprise seule, tant pour des raisons de coût que de cas d’utilisation, ne peut tester. À cet égard, il est intéressant pour l’entreprise de dresser une cartographie de l’ensemble de ces acteurs puisque chacun d’eux a un rôle à jouer dans l’innovation.
   * S’agissant des projets existants&nbsp;: la publication des brevets permet d’avoir un état de la technique. La plupart des États disposent d’une base de données répertoriant l’ensemble des brevets déposés. Parallèlement, l’ouverture des données des administrations et des entreprises permet d’avoir une idée plus précise de ce qui se fait dans l’écosystème. Ainsi, l’organisation pourra déterminer les modalités d’interaction avec son écosystème en identifiant au sein de sa propre organisation quelles sont les activités clés, les points forts et les faiblesses pour un projet donné. Puis, à l’aide de la cartographie des acteurs, identifier ceux ayant les compétences complémentaires nécessaires.

### De la protection à la gestion des biens immatériels


#### Droits de propriété intellectuelle

Un des outils privilégiés pour gérer les potentiels risques liés à la propriété intellectuelle est celui de la contractualisation. Accepter que les informations, logiciels, méthodes et autres savoirs ou objets circulent dans l’écosystème ce n’est pas abandonner toute propriété. Par exemple, lorsqu’un programme intéressant est utilisé pour un projet et que celui-ci n’appartient pas à l’entreprise, il est important de vérifier sa licence et de connaître les droits qui sont accordés. Il est inutile de développer de nouveau, en interne, un programme qui fera la même chose, il suffit simplement de respecter la licence de celui qui nous intéresse. Tous les moyens légaux tels que les chartes, licences et autres contrats sont des garanties pour la circulation des inventions tout en évitant les abus. Les compétences externes auxquelles font appel les entreprises peuvent être valorisées au travers d’une politique de propriété intellectuelle favorable à la circulation des savoirs et donc reposant sur le contrat. 

À ce titre, le libre et l’Open Source ont été construits à partir de la pratique contractuelle puisqu’ils décrivent l’un et l’autre un certain type de licences.

Plus encore, le projet GNU est né de l’impossibilité d’accéder au code source en vertu des termes d’une licence propriétaire. Richard Stallman a ainsi participé à stigmatiser les limites liées à une innovation fermée. Auteur du logiciel GNU Emacs, Richard Stallman lui associa en 1985 une licence spécifique intitulée la GNU Emacs General Public License&nbsp;: sensiblement plus longue que les licences précédentes, elle définissait avec précision le cadre permettant la réutilisation de la licence et obligeait à conserver la licence sur toutes les versions dérivées (ou contenant une partie) du logiciel. Cette protection découlait essentiellement de déboires que connut Stallman lorsqu’il souhaita réutiliser une version modifiée de son logiciel Emacs (Gosling Emacs) et qu’il se vit opposer le copyright de l’auteur de cette dernière – situation qui le contraignit finalement à tout réécrire pour ne pas être inquiété par la société qui avait acheté les droits. Conditionnant la licence (au sens de permission) à une certaine réciprocité, il se servit ainsi de ses propres droits d’auteur pour limiter toute réappropriation par un autre. Ressentant comme une menace la combinaison de code libre et non libre, cette licence formalisa la notion de copyleft par laquelle l’ajout ou la jonction d’un élément quelconque à un programme sous une telle licence doit se faire de sorte que la version résultant de cette opération soit régie par les mêmes termes (et donc également libre). C’est de là que sont nées la Free Software Foundation ainsi que les licences libres telles que les licences GNU GPL.

Par la suite, le développement commercial de la licence libre a engendré l’émergence du mouvement Open Source. Ce mouvement est issu des réticences du monde des affaires à l’égard du  *free software* dans la mesure où le terme « free » renvoyait à la fois au libre et au gratuit. L’Open Source a vocation à s’attacher à l’économie du logiciel libre. L’Open Source Initiative (OSI) a été mise en place afin d’encadrer la pratique des licences Open Source décrites à l’aide des critères suivants&nbsp;: liberté de redistribution, intégrité du code source, liberté de modifications, accès au code source, non-discrimination contre les personnes et les groupes, non-discrimination contre les champs d’application, licence non-spécifique à un produit, pas d’obligation de contracter une licence supplémentaire pour la distribution du logiciel, neutralité du point de vue technologique et enfin, interdiction d’imposer des restrictions à la distribution d’autres logiciels.

Le succès de ces licences a fait émerger les mouvements d’Open Data et d’Open Content qui sont le résultat de la transposition des principes liés au libre et à l’Open Source. 

De cette manière, favoriser la pratique contractuelle permet d’avoir une gestion plus souple des biens immatériels mais aussi de conforter la stabilité du modèle ouvert en lui apportant un cadre juridique adapté et personnalisé. Par ailleurs, certaines licences tendent à devenir des standards ce qui facilite leur réutilisation. 


#### Gestion de la confidentialité

Si la gestion des droits de propriété intellectuelle est simplifiée grâce au cadre légal des licences, celle de la confidentialité est plus fine que dans le cadre du modèle fermé. En effet, l’Innovation Ouverte suggère une inversion de logique&nbsp;: ce n’est plus la confidentialité qui s’impose par défaut, mais l’ouverture. L’organisation devra opérer un arbitrage entre les informations qu’il est envisageable de partager et celles qui ne le sont pas en prenant en compte le potentiel que représente la diminution des asymétries d’information. Ceci explique en partie les différents niveaux d’ouverture selon les secteurs d’activité. En effet, certains secteurs sont plus sensibles que d’autres. On peut citer notamment le secteur bancaire, de l’énergie ou encore de la défense.


## Automatiser et systématiser la collaboration 

L’entreprise doit s’ouvrir aux acteurs de l’écosystème en déployant les mécanismes d’Open Data, d’Open Source et d’Open Hardware, ce qui permet de systématiser la collaboration tout en adoptant un modèle économique adapté. 

En termes d’étapes, la collaboration dans les stratégies d’Innovation Ouverte peut avoir lieu à différents stades du développement du projet. Il est possible de collaborer en phase de recherche, en phase de développement, de commercialisation mais aussi il peut arriver de collaborer pour orchestrer une sortie de marché. On constate souvent que l’étape d’idéation et de développement (ce qu’on nomme R\&D) est la plus favorable pour la collaboration. En effet, surtout dans les secteurs de hautes technologies, où les coûts pour la recherche peuvent être très élevés, il est indispensable de travailler en concert avec des partenaires pour&nbsp;:

   * réduire les coûts d’investissement,
   * partager les risques,
   * favoriser l’apprentissage inter-organisationnel.

L’Innovation Ouverte peut donc être systématisée grâce à une collaboration à toutes les étapes du projet. Mais pour être pérennisée et entrer dans une logique d’automatisation au sens de réflexe, il est aussi important d’adosser une stratégie d’Innovation Ouverte à un business model spécifique. Celui-ci peut se trouver être différent de celui en place et nécessiter donc une refonte profonde des systèmes de valorisation de l’innovation.

### Open Data, Open Source, Open Hardware, Open Content

Le concept d’innovation ouverte se traduit sous une multitude de formes, des plus classiques aux plus disruptives, des plus contrôlées aux plus ouvertes. Ce sont néanmoins les modèles ouverts (Open Source, Open Data, Open Hardware, etc.) qui sont aujourd’hui les plus représentatifs et pertinents compte tenu de la pérennité qu’ils confèrent à l’innovation grâce à la confiance qu’ils inspirent aux acteurs en supprimant les menaces de réappropriation et à l’agilité qui les caractérise et leur permet d’évoluer en même temps que la société.

Ces modèles nourrissent l’innovation et contribuent à la réinventer. Ainsi, toute démarche d’innovation ouverte doit nécessairement prendre en compte ces tendances, par réaction aux pratiques qui se généralisent, mais aussi afin de ne pas marginaliser de petites entreprises incapables de supporter le coût de licences élevées, faciliter l’entrée de nouveaux acteurs ou encore capitaliser sur les investissements réalisés. La finalité de leur adoption est généralement très spécifique à l’organisation, même si quelques grandes orientations se détachent&nbsp;:

   * L’Open Source constitue aujourd’hui une partie significative de la partie logicielle de tout système d’information et contribue aux processus métiers de la plupart des organisations. Ce modèle permet par ailleurs de lever le maximum de verrous à l’entrée afin d’attirer une communauté large de développeurs&nbsp;;
   * L’Open Data (ou données ouvertes) répond à des logiques économiques et de transparence et renforce les projets de Big Data tout en ajoutant une dimension collaborative et de partage alternatif. Une animation spécifique à l’Open Data est nécessaire, afin de favoriser l’innovation ouverte sur la base de la réutilisation voire de la contribution aux jeux de données de l’organisation&nbsp;;
   * L’Open Content encourage le partage de contenus, en permettant leur copie, leur réutilisation, leur modification etc. Le mouvement a connu un essor particulier avec la création des licences Creative Commons utilisées par le projet Wikipédia&nbsp;;
   * L’Open Hardware permet une optimisation des coûts et le bénéfice de matériels parfaitement adaptés aux besoins (Arduino, Open Compute, etc.). Cette démarche a démontré son intérêt dans le cadre de projets nécessitant des investissements matériels conséquents ainsi qu’un travail important d’ingénierie dans un contexte de forte interopérabilité au sein d’une chaîne de fournisseurs complexe. Plusieurs initiatives d’Open Hardware ont fait leurs preuves (voir notamment les usages du CERN, le projet opencompute.org ou encore Open Airbus Cockpit)&nbsp;;
   * Les Open API facilitent l’exploitation des données ouvertes par les développeurs en s’appuyant massivement sur des standards ouverts et connus de tous.

Ainsi, toute organisation confrontée à ces divers mouvements doit réfléchir au positionnement stratégique qu’elle adopte pour chacun d’entre eux, et formaliser ensuite cette prise de position en une série de règles et vade-mecum qui l’implémenteront. À noter qu’une externalisation totale semble difficile en raison de l’interpénétration des notions juridiques et techniques d’une part, et de la criticité de ces enjeux d’autre part.

La gouvernance passe généralement par la création d’une entité ad hoc (référent Open Source, comité Open Source, etc.) ou, lorsque possible, la modification de structures existantes pour y intégrer ces besoins. De nombreux paramètres entrent dans cette équation, mais il est généralement conseillé, lorsque la taille le permet, de commencer par la mise en place d’entités autonomes, de sorte qu’elles s’éclipsent une fois la nouvelle culture infusée et les réflexes intégrés — au risque sinon de buter sur la résistance des collaborateurs (surtout dans le cas du passage d’une politique de licence traditionnelle à une politique ouverte&nbsp;: la longue domination du paradigme antérieur, engendrant de nombreux réflexes et habitudes profondément ancrés, peut entraîner le rejet irrationnel de ce mode alternatif d’édition).

Quelle que soit l’organisation choisie, il est nécessaire que cet accompagnement s’appuie sur des relais locaux (scouts), des correspondants aptes à faire remonter et redescendre les informations utiles.

À la fois structure de référence et structure inter-services, un tel organisme permet de mutualiser le travail d’accompagnement, de rationaliser les décisions, de définir des politiques globales et d’automatiser le traitement des questions. Par ailleurs, pour qu’une telle structure puisse être suivie d’effets, il est nécessaire qu’elle soit facilement joignable, qu’elle bénéficie d’un pouvoir décisionnel et, pour être efficace, qu’elle soit soutenue par un membre de la direction.


### Favorisent les rencontres et la proximité
 
On ne peut prétendre faire de l’innovation ouverte que si cette pratique est culturellement encrée dans l’entreprise et qu’il y a une réelle stratégie de systématisation de la collaboration. Afin de parvenir à un tel résultat, il existe des actions et des lieux qui favorisent les rencontres et la création de synergies.


#### Les pôles de compétitivité

Les pôles de compétitivité regroupent des entreprises, des laboratoires, des collectivités et tout un ensemble d’acteurs qui souhaitent innover dans divers domaines. Ces pôles sont organisés par thématiques et sont liés à un territoire, ce qui favorise les rencontres de proximité. Il existe aujourd’hui plus de 70 pôles répartis sur l’ensemble du territoire français [^competgouv]. En adhérent à un pôle de compétitivité, il est possible d’identifier des financeurs pour lancer un projet, bénéficier de formations pour savoir comment construire son écosystème ou constituer un consortium, rencontrer des partenaires avec qui lancer une idée nouvelle ou encore rejoindre un projet en cours.

[^competgouv]: Voir sur [competitivite.gouv.fr](http://competitivite.gouv.fr/).


#### Les consortiums

Les consortiums sont des regroupements d’entreprises où il est possible de mettre en oeuvre un projet d’innovation ouverte. Sans statut juridique figé, les consortiums sont des espaces de coopération flexibles et évolutifs mais qui n’en sont pas moins structurés autour de règles et de chartes, notamment en ce qui concerne les questions de la propriété intellectuelle des objets mis en commun ou produits collectivement. En effet, dans le cadre d’une mise en place de tiers lieux destinés à favoriser la création, l’innovation et la collaboration, il est important de prévoir en amont des règles claires relatives à l’usage des ressources et à la propriété des créations produites (notamment en termes de propriété intellectuelle). Celles-ci peuvent être formalisées au sein d’une charte courte qui constitue le cadre général dans lequel les participants entendent collaborer. Il serait possible de s’inspirer d’un règlement du type  *Fab Charter*.

#### Les laboratoires de co-création

Les Fab Labs désignent des tiers lieux régis par un ensemble de principes définis par le MIT (Massachusetts Institute of Technology) et sur la base desquels ces derniers labellisent les laboratoires qui doivent&nbsp;:

   * être conformes à la *Fab Charter*&nbsp;,
   * participer à des initiatives extérieures et ses membres doivent collaborer avec ceux d’autres Fab Labs,
   * être partiellement ouverts au public.

Les *Living Labs* regroupent un ensemble d’acteurs privés, publics, associatifs et individuels (citoyens, usagers) pour placer l’usage et le quotidien au centre de l’innovation. Ils tendent à prendre en compte l’échelon local dans le processus d’innovation. Dans ce contexte, l’usager est porteur de l’innovation. 


#### Les clusters

Le cluster désigne un lieu géographique identifié au sein duquel un ensemble d’acteurs hétérogènes coexistent. «&nbsp;Un cluster qui fonctionne est pris dans un cercle vertueux. Comme dans une économie de réseau, l’arrivée d’un nouvel acteur connecté augmente la valeur du réseau pour tous ceux qui y participaient déjà, et renforce son intérêt pour tous ceux qui pourraient envisager de le rejoindre.&nbsp;»[^clustersaclay]


[^clustersaclay]: Présentation&nbsp;: *Cluster Paris-Saclay&nbsp;: start-up, grandes entreprises et centres de R&D*, sur [epaps.fr, doc. pdf](http://www.epaps.fr/wp-content/uploads/2014/12/XDGA_SACLAY_JOURNAL_4_1124.pdf).


#### Exemple&nbsp;: le Pôle Aquinetic 

Le Pôle Aquinetic a été créé sous la forme d’une association «&nbsp;loi 1901&nbsp;» dont l’objectif est «&nbsp;l’émergence, le portage, la reconnaissance, la promotion et le développement d’un pôle régional de compétences scientifiques, techniques, et industrielles pour l’innovation ouverte et libre ainsi que pour les technologies libres.&nbsp;»

Le principe fondateur d’Aquinetic consiste à tirer parti du modèle d’innovation libre pour créer et développer des emplois à haute valeur ajoutée de façon déconcentrée. Là où l’industrie de production de masse vise à satisfaire des besoins standardisés et, une fois ceux-ci définis, mise sur la délocalisation de grandes unités de production vers des pays à bas niveaux sociaux pour générer de la marge sur ces produits, les technologies libres permettent à toute personne ayant un besoin spécifique de construire, par agrégation de briques préexistantes, la solution à son problème. S’il existe ailleurs des personnes aux besoins similaires, la solution peut être répliquée et adaptée pour devenir un produit valorisable, que ce soit de façon monétaire ou bien grâce à des partenariats avec des contributeurs potentiellement en situation de coopétition. Là où les modèles industriels classiques opposent le *techno push* (fourniture de solutions techniques potentiellement inadéquates au marché) et le *market pull* (demande par le marché de produits répondant à leurs besoins), les modèles d’innovation libre permettent le *techno pull* (adoption de technologies élaborées pour résoudre des problèmes voisins) et le *market push* (diffusion d’un produit, d’un marché de niche à un autre, par capillarité et adaptation de l’usage).

Alors que le logiciel libre est mondial par essence, il a été décidé de restreindre le périmètre d’action d’Aquinetic au cadre régional, pour plusieurs raisons.

La première tient au modèle que cherche à promouvoir Aquinetic, à savoir la capacité de maintenir et de développer des emplois à haute valeur ajoutée en tous types de milieux&nbsp;: urbain, rurbain mais aussi rural. L’Aquitaine, puis la Nouvelle-Aquitaine, de par sa diversité, constitue un terrain idéal pour cela. La deuxième raison tient à la capacité de déplacement des acteurs&nbsp;: le but d’Aquinetic est de permettre les rencontres et échanges informels, à la manière des pôles de compétitivité, ce qui ne peut être mis en œuvre de façon souple et peu coûteuse qu’au plus à l’échelle régionale. En troisième lieu, le soutien affiché du Conseil régional au logiciel libre, par la nomination d’un chargé de mission dédié au sein de la Délégation aux usages du numérique (DTIC), permet un meilleur accès des projets labellisés par le Pôle Aquinetic aux financements régionaux, à l’image (mais à une bien moindre échelle) de l’accès au FUI permis par les pôles de compétitivité.

Le travail effectué par l’association auprès des porteurs de projets ayant très souvent porté sur l’accompagnement à la définition du modèle économique de leur entreprise, il est apparu nécessaire de rationaliser cette étape en la structurant de façon formelle. C’est pour cela qu’Aquinetic, en partenariat avec la pépinière d’entreprises Bordeaux Unitec, a ouvert à Pessac en 2014 « La Banquiz », premier accélérateur européen d’ENL. La Banquiz accueille tous les six mois une promotion de trois à quatre entreprises, autour d’un programme décliné en cinq thématiques&nbsp;: introduction à l’écosystème des technologies libres en Aquitaine, introduction aux modèles économiques du libre, définition de la stratégie de l’entreprise, établissement d’un prévisionnel financier, gestion et pilotage. Devant le succès rencontré, une deuxième Banquiz a été ouverte en début d’année 2016 à Pau, sur le site Hélioparc, conformément au principe que les entreprises doivent autant que possible rester sur le territoire qui leur a donné naissance. D’autres accompagnements sont envisagés sur les ex-régions Limousin et Poitou-Charentes.


#### Adhésion à des organisations (Open Innovation Network, Patent Pool)

Les organisations professionnelles et autres associations sont des lieux privilégiés pour rencontrer des acteurs nouveaux, aux compétences diverses et complémentaires. Ces acteurs peuvent venir enrichir l’écosystème en construction sur lequel va s’appuyer le déploiement de la stratégie d’innovation ouverte.


# Transformer l’organisation de l’entreprise 

L’innovation ouverte a des conséquences structurelles pour l’entreprise puisqu’elle nécessite de favoriser les échanges entre salariés et d’encourager ces derniers à interagir avec l’environnement extérieur. Associer les salariés à la démarche d’ouverture permet non seulement à l’entreprise de s’inscrire dans un processus disruptif pour l’organisation mais aussi de familiariser ces derniers aux concepts de l’Innovation Ouverte et ainsi de valoriser au mieux leurs compétences. Ce faisant, ils seront à même de jouer un rôle clé dans la diffusion du modèle ouvert.
 
À ce titre, ils doivent être perçus par l’entreprise à la fois en tant qu’individus et en tant que collectifs humains. En effet, la première approche permet de mettre en avant la créativité de chacun au-delà de ses fonctions dans l’organisation avec l’idée d’interaction avec l’extérieur. Tandis que la seconde approche, accentue l’aspect collaboratif du processus d’innovation en faisant des salariés d’une entreprise une véritable communauté. Il revient à l’entreprise de répandre cette culture au sein de l’organisation.

## Favoriser l’interaction entre collaborateurs

Au-delà des activités métier des entreprises, l’Innovation Ouverte a un impact structurel sur leur organisation interne puisqu’elle vise à créer des interactions entre différentes fonctions. En effet, l’adoption du modèle ouvert ne doit pas être limitée au département de R\&D appuyée par le département juridique mais impose d’impliquer l’ensemble des départements de l’entreprise dans le processus d’innovation. À cet égard, le département juridique ne doit plus être considéré comme une fonction support mais comme un potentiel vecteur dans la conduite du changement, comme tout autre département.

Cela peut s’analyser au travers de la nécessité de modifier la répartition des départements au sein de l’entreprise en vue de mélanger les fonctions. Ainsi, il faut décloisonner les fonctions de l’entreprise pour favoriser l’intelligence collective qui renvoie à la "*capacité à maximiser la liberté créatrice et l’efficacité collective*", selon Corinne Weber, en référence à Pierre Lévy. 

### Responsabilité sociétale des entreprises

Au travers de la responsabilité sociétale de l’entreprise (RSE), il est possible de fédérer plus facilement les salariés de manière transdisciplinaire autour d’un projet commun dont l’approche sociétale est partagée par tous. À cet égard, Claire Martin relève qu’il y a "*une appétence des salariés pour ce genre de contributions. Ils sont fiers quand l’entreprise contribue au bien commun, et quand en plus ils y sont associés*". Les entreprises sont d’autant plus poussées à s’inscrire dans une telle démarche que la RSE fait l’objet d’une norme ISO (norme ISO 26000). 
(Claire Martin, Bien Commun et Entreprise, Regards croisés sur les nouvelles responsabilités de l’entreprises, Communication et Entreprise, ujjf). 

En outre, la responsabilité sociale de l’entreprise est perçue comme un levier de transformation au même titre que l’Innovation Ouverte. Ce lien entre les deux est d’autant plus prégnant que l’innovation sociétale peut amener à l’innovation ouverte et inversement. 

### L’innersourcing

Les méthodes d’*innersourcing* permettent de renforcer l’interaction entre les collaborateurs. Elles consistent à tirer profit des méthodes de l’Open Source en les transposant au sein de tout type d’industrie et plus spécifiquement à la gouvernance et au processus de développement. 

Exemple concret&nbsp;: une entreprise souhaite développer un logiciel Open Source en s’appuyant sur l’ensemble des collaborateurs et au-delà pour créer sa propre communauté&nbsp;:

   1. cibler son projet et sa taille&nbsp;: commencer avec des petits projets et définir les étapes du processus&nbsp;;
   2. définir la charte de fonctionnement du projet&nbsp;;
   3. déterminer ses outils collaboratifs&nbsp;: «&nbsp;Tools are specific concrete things that a culture has decided is a way to improve a process&nbsp;»[^tools]&nbsp;;
   4. mesurer l’impact&nbsp;: analyse technique en évaluant le coût du code, la standardisation du développement et l’amélioration de la productivité mais aussi mesurer l’impact pour l’entreprise sur le plan organisationnel.

[^tools]: Bryan Behrenshausen, «&nbsp;What does it mean to change company culture?
Culture trumps tools—every time&nbsp;», [opensource.com](https://opensource.com/open-organization/16/9/culture-trumps-tools-every-time), 27/09/2016. 


Ces méthodes tendent à propager une nouvelle vision de la création de valeur dans laquelle «&nbsp;la qualité de vie et l’expression du potentiel de chacun au sein du collectif deviennent primordiales. Ainsi, un projet commun peut tout à fait rejoindre un projet individuel. C’est plus simple à réaliser une fois que l’on a constaté et compris que les personnes sont beaucoup plus efficaces et agiles lorsqu’elles travaillent sur des sujets qui les passionnent. Il faut s’éloigner des visions qui animaient le taylorisme et le fordisme… Donner de la place à l’individu est l’une des clés du succès collectif.&nbsp;»[^bjbiencommentre]

[^bjbiencommentre]: Benjamin Jean,dans *Bien Commun et Entreprise, Regards croisés sur les nouvelles responsabilités de l’entreprises*, Communication et Entreprise, ujjf, 2016.  

L’*innersourcing* entraine un changement profond dans la représentation du monde de l’entreprise. L’un des points les plus importants porte notamment sur le comportement social et l’absence de hiérarchie. La notion d’égalité est centrale et c’est sur elle que s’appuie l’ensemble des principes de production. La considération que tous les êtres humains sont égaux dans leur statut social génère une décentralisation des pouvoirs. Il n’y a pas de limitation de l’expression du fait de sa position dans l’entreprise. Les décisions sont basées sur un consensus et tout le monde part sur un pied d’égalité. Les retours d’expérience et les différents points de vue sont valorisés.


### La diversification des profils recherchés 

Cette vision a des conséquences sur la politique de ressources humaines en termes de recherche de profils. Dans ce contexte, les dirigeants et managers doivent avoir des connaissances multiples et transversales puisqu’il ne s’agit plus de gérer des équipes mais des collaborateurs aux fonctions variées. Les collaborateurs seront davantage perçus au travers d’expériences non nécessairement issues du milieu professionnel ou de leur formation. Leur autonomie et leur proactivité seront des qualités essentielles et leur côté entrepreneur primordial. Ceci est conforté par le fait qu’aujourd’hui les individus sont de moins en moins cantonnés à un seul métier. 

 Il faut regarder le collaborateur pour ce qu’il est et non plus en contemplation de sa fonction. 

## Favoriser l’interaction des collaborateurs avec l’environnement extérieur

L’innovation ouverte conduit à repenser la dichotomie ancienne existant entre la recherche académique et la R\&D industrielle. La première avait été pensée comme un moyen de mutualiser les recherches fondamentales, en les finançant par l’impôt, tandis que la seconde était réalisée de façon confidentielle au sein des entreprises, sur fonds privés. Se placer dans une démarche d’innovation ouverte revient donc à réorienter le modèle de R\&D de l’entreprise vers plus de mutualisation, ce qui suppose plus d’ouverture. Tout comme dans la recherche académique, il s’agit alors pour les membres de l’entreprise de tisser des liens avec des homologues d’autres entreprises travaillant sur des sujets identiques ou connexes.

L’organisation de ces réseaux obéit à une logique spécifique. À l’image des pratiques du monde académique, les collaborations doivent s’établir entre acteurs se reconnaissant de force égale et partageant les mêmes valeurs, faute de quoi la relation serait déséquilibrée et conduirait, à moyen terme, à des frustrations dommageables à la dynamique collaborative.

La bonne mise en œuvre de ces réseaux d’interaction nécessite de mettre en place des outils destinés à la recherche des personnes ressources, tant en interne qu’en externe, afin de systématiser les collaborations. L’identification des compétences internes peut prendre la forme  d’un réseau social d’entreprise, ou de tout autre outil permettant à chacun de déclarer les compétences qu’il possède et qui pourraient être recherchées par d’autres. En externe, ces compétences peuvent être exposées aux partenaires pressentis, et faciliter la construction de groupes-projets.

### Favoriser l’intrapreneuriat 

On regroupe sous le terme d’« intrapreneuriat » un ensemble de pratiques organisationnelles mises en œuvre au sein de l’entreprise et visant à laisser s’exprimer les compétences et la créativité des salariés. Parmi ces mesures, on peut citer le fait de laisser aux salariés 10 à 15% de «&nbsp;temps libre&nbsp;» pour travailler sur des projets « personnels », à la condition que les résultats de ces projets soient ensuite évalués par le service de valorisation de l’entreprise, qui décidera s’ils seront pris en compte en tant que produits issus de l’entreprise ou pourront être transférés à un tiers, tel qu’en particulier l’ex-salarié qui déciderait de créer sa *spin-off*. Le réseau social d’entreprise peut être utilisé par les salariés pour recruter les compétences dont ils peuvent avoir besoin pour résoudre tel ou tel sous-problème de leur projet. C’est ainsi qu’a été conçu le Post-It au sein de la société 3M, devenu l’un des *best-sellers* de l’entreprise, issu du besoin d’un employé musicien agacé que ses partitions s’envolent lorsqu’il jouait en public.

### Sensibiliser à la participation des barcamps et hackathons

Les barcamps et les hackathon sont des ateliers. Le premier renvoie à la situation dans laquelle les participants sont tous acteurs et coconstruisent l’organisation de l’atelier autour d’un sujet donné. Le second est issu du milieu du logiciel. Initialement, il s’agissait de rassembler lors d’un événement un ensemble de développeurs en vue de faire un programme déterminé. Désormais, il a trouvé de nouvelles applications notamment au sein de la Civic Tech, avec le lancement de l’initiative Open Democracy Now! dont l’objectif est d’aboutir à la création d’outils Open Source en matière de démocratie participative. 

La participation des collaborateurs à des événements extérieurs de type barcamp et hackathons leur permet de mieux comprendre le fonctionnement de la communauté du logiciel ou du produit qu’ils utilisent et donc&nbsp;: d’une part, de s’enrichir de ces connaissances, d’autre part, d’apporter leur aide dans le processus de développement, de telle sorte qu’ils deviennent de véritables acteurs.

Ceci pouvant être valorisé par la suite au travers de la mise en place de boîtes à idées dans l’organisation.

### Créer des boîtes à idées 

Il faut qu’en interne les compétences du salarié qui ne sont pas liées à son poste puissent être valorisées. Cette valorisation peut passer par la mise en place au sein de l’entreprise de plateforme ayant fonction de boîtes à idées. Tout le monde y trouve son compte tant les collaborateurs que l’entreprise elle-même&nbsp;: 


   * Pour les collaborateurs&nbsp;: leur créativité et leur rôle dans l’organisation sont mis avant. Ces boîtes à idées jouent également un rôle important pour fédérer les collaborateurs et rétablir du lien social entre eux.
   * Pour l’entreprise&nbsp;: qui est mieux à même de connaître les besoins liés à son poste et plus largement à l’organisation sinon le salarié&nbsp;? Sa connaissance de l’entreprise et la prise en considération du salarié au-delà de sa fonction dans l’organisation c’est-à-dire en tant qu’individu sont source de richesse en termes de savoirs pour l’organisation. Les boîtes à idées au sein des organisations sont des outils essentiels à la conduite du changement. De plus, elles permettent également de réinstaurer un lien entre les décisionnaires et les collaborateurs et donc d’apaiser le climat social de l’entreprise. C’est une manière de gommer la hiérarchie pyramidale. Enfin, elles ont un impact économique favorable pour l’entreprise. Ainsi, Renault a affirmé avoir économisé 135 millions d’euros suite à la mise en place d’une boîte à idées[^boitidee].
   * 
[^boitidee]: Olivier Dutel, «&nbsp;Comprendre les intérêts de la boîte à idée.&nbsp;», *[blog Les temps changent](http://les-temps-changent.com/comprendre-les-interets-de-la-boite-a-idee/)*, 19/02/2013.


### Encourager la participation aux communautés Open Source 

Au même titre que les boîtes à idées, la participation des salariés à projets Open Source qui sont extérieurs à l’entreprise permet à ces derniers de faire valoir leurs compétences au-delà de l’organisation dans laquelle ils travaillent, tout en offrant la possibilité à l’entreprise d’avoir une meilleure connaissance de son environnement extérieur et de réinjecter potentiellement des savoirs externes.

Plus spécifiquement, la participation des salariés au développement d’outils Open Source utilisés en interne permet&nbsp;:

   * pour les collaborateurs&nbsp;: de concevoir un outil plus adapté à leur travail quotidien&nbsp;;
   * pour l’entreprise&nbsp;: de pérenniser l’investissement qu’elle fait dans l’outil en étant au plus proche du développement.


Il est important, pour valoriser au mieux les compétences, que l’entreprise&nbsp;:

   * rende public les développements auxquels ses salariés contribuent, pour encourager la visibilité desdites contributions&nbsp;; 
   * favorise la création par l’entreprise de communautés en s’appuyant par exemple sur la formation de groupe de travail dédié à un logiciel ou une réflexion. On peut prendre l’exemple de la Mutualisation Interministérielle pour une bureautique ouverte  (MIMO)[^mimo] groupe de travail visant à mutualiser l’ensemble des ministères participant à la migration sur LibreOffice. Cela a permis de mettre en commun non seulement les problématiques liées à la migration, mais également les outils support de cette migration. Ce groupe a également pu interagir avec la communauté de développement du logiciel en participant à des organes de gouvernance ou à la chaîne de production du logiciel&nbsp;;
   * recrute parmi les membres des communautés Open Source ou souscrivent auprès des sociétés certifiées par les projets open source&nbsp;; 
   * sollicite la mise en place de contributor agreement&nbsp;: contrat qui règle le sort des droits de propriété intellectuelle lié à la contribution à un projet. Néanmoins, ces propos sont à nuancer puisque cela peut constituer un frein à la contribution en obligeant à partager des droits notamment le droit moral ce qui n’est pas conforme au cadre juridique européen. Cela dépend également de la culture de la communauté. 

[^mimo]: Voir Jounral Officiel&nbsp;: MIMO. Sur [www.journal-officiel.gouv.fr](http://www.journal-officiel.gouv.fr/mimo/).


### Repenser les espaces 

En outre, afin de permettre aux collaborateurs d’interagir avec l’environnement extérieur, il est nécessaire de repenser les espaces de l’entreprise notamment au travers de la mise en place d’espaces de co-working. Nés en 2005 à San Francisco, il s’agit d’ouvrir des espaces de bureaux partagés. Conçu à  l’origine pour rompre l’isolement des travailleurs indépendants, il présente de réelles opportunités pour l’entreprise accueillante. En effet, ceux-ci sont de nature à faciliter les échanges de compétences entre collaborateurs de l’entreprise avec ceux d’une entreprise tierce et plus particulièrement des personnes ayant un profil d’entrepreneur ou freelance.

La création de labs innovation au sein des entreprises constituent également un moyen de tourner les collaborateurs vers l’extérieur. C’est notamment dans cette optique qu’a été créé le Lab Orange. Ce dernier organise par exemple un Access Camp, barcamp sur l’accessibilité, le logiciel libre et Open Source.

Plus largement, il peut s’agir d’implanter l’entreprise ou une de ses composantes sur un site ouvert combinant espaces de co-working et campus  universitaire. À ce titre, le cluster Paris-Saclay a été conçu de sorte à faire cohabiter le milieu étudiant et universitaire avec celui de l’entreprise ainsi que le milieu de la recherche scientifique, et ce logique pluri-disciplinaire. Les objectifs recherchés peuvent être résumés de la sorte&nbsp;: «&nbsp;Attirer des entreprises innovantes grâce aux potentiels scientifiques réunis autour du plateau de Saclay, multiplier les passerelles permettant les transferts de technologies, et créer un environnement propice à la naissance et à la croissance des start-up…&nbsp;»[^saclay2]. Dans cette logique, EDF Lab est présent sur le site Paris-Saclay, ce qui permet de renforcer considérablement l’interaction de ses collaborateurs avec l’ensemble des acteurs extérieurs également présents sur le plateau.

La participation de l’entreprise à des consortiums, des pôles de compétitivité ainsi qu’à des Living Labs permet également aux salariés d’être plus ouverts à leur environnement extérieur. 

[^saclay2]: Présentation&nbsp;: *Cluster Paris-Saclay&nbsp;: start-up, grandes entreprises et centres de R&D*, sur [epaps.fr, doc. pdf](http://www.epaps.fr/wp-content/uploads/2014/12/XDGA_SACLAY_JOURNAL_4_1124.pdf).



