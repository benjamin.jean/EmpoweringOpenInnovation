# Empowering Open Innovation. Du rôle et de la place des modèles ouverts

Partenaires : Aquinetic, Inno3

![aquinetic](images/Aquineticx150.png)

![inno3](images/Inno3x150.png)

Sous la direction de

   * Benjamin Jean, Fondateur et Président de Inno³, Co-président du Paris Open Source Summit 2016

Autres contributeurs :

   * Amel Charleux : Doctorante à l'Université de Montpellier
   * Sophie Gautier : Responsable gestion des communautés à Inno3
   * Laure Kassem : juriste à Inno3
   * François Pellegrini : Professeur d'informatique et vice-président délégué au numérique à l'Université de Bordeaux, Co-président du pôle Aquinetic


**Licence : CC By-SA 4.0**

